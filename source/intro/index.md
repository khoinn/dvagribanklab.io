---
title: intro
date: 2018-06-21 12:40:47
layout: thongtin
slideshow: true
---


## thư ngỏ

*Kính gửi Quý khách hàng*

Lời đầu tiên, chúng tôi xin thay mặt Công ty TNHH Một thành viên Dịch vụ Ngân hàng Nông nghiệp Việt Nam gửi tới Quý khách hàng lời chào trân trọng cùng lời chúc sức khỏe và thành công.

Nhân dịp này, chúng tôi muốn được gửi lời cảm ơn chân thành đến Quý khách hàng vì những tình cảm quý báu và sự tin tưởng đã dành cho chúng tôi trong suốt thời gian qua. Chính sự hợp tác tốt đẹp đã hỗ trợ chúng tôi rất nhiều trong việc duy trì và phát triển hoạt động sản xuất kinh doanh, góp phần mang lại thành công cho Công ty và đóng góp một phần vào sự lớn mạnh của ngôi nhà chung Agribank.

Trên tinh thần hợp tác hỗ trợ để cùng nhau phát triển, chúng tôi mong muốn được là đơn vị cung cấp các dịch vụ: In ấn, Ngân quỹ, và Quảng cáo phục vụ cho hoạt động của Quý khách hàng. Mục tiêu của chúng tôi là đem đến cho Quý khách hàng dịch vụ chất lượng cao nhất với chi phí hợp lý nhất.

Chúng tôi cam kết đảm bảo chất lượng dịch vụ, giữ vững uy tín, để khẳng định sự đúng đắn của Quý khách hàng khi lựa chọn dịch vụ của chúng tôi.

Một lần nữa, xin trân trọng cảm ơn và chúng tôi hy vọng sẽ được tiếp tục hợp tác vì sự thịnh vượng, phát triển của Quý Khách hàng cũng như toàn hệ thống Agribank.

*Trân trọng kính chào.*

CHỦ TỊCH CÔNG TY

**Trần Duy Hưng**

