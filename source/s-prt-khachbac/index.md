---
title: s_prt_khachbac
date: 2018-06-21 13:15:52
---


1. NH Nhà nước (SBV): In séc, Catalogue nhận biết tiền thật, giả..
1. NH Nông nghiệp & PTNT Việt Nam ( AGRIBANK) một số chi nhánh các tỉnh phía Bắc (160 chi nhánh và PGD): In séc, Sổ TK, Thư bảo lãnh, giấy nộp tiền, ủy nhiệm chi, giấy ATM, Tạp chí TT,số ấn chỉ, tờ rời ...v...v...
1. NH TMCP Ngoại thương Việt Nam (VCB) : In séc, Thẻ tiết kiệm, Thư bảo lãnh
1. NH TMCP ĐT & PT Việt Nam (BIDV) : In séc
1. NH TMCP Hàng Hải (MARITIME BANK , MSB): In séc
1. NH TMCP Sài Gòn – Hà Nội (SCB):In séc
1. NH TMCP Bưu điện Liên Việt (LienVietPostBank): In séc, Sổ tiết kiệm, Thư bảo lãnh
1. NH VID PUBLIC BANK: In séc, Sổ tiết kiệm
1. NH Bảo Việt (BVB) : In séc, Sổ tiết kiệm, Thư bảo lãnh
1. NH Đại Dương ( OCEAN BANK): In séc, Sổ tiết kiệm
1. NH Bắc Á (NASB): In séc, sổ tiết kiệm, Thư bảo lãnh
1. NH Dầu khí Toàn cầu ( GP BANK): In séc, Sổ tiết kiệm
1. NH TMCP Quân Đội (MILITARY BANK , MB): In séc, Sổ tiết kiệm
1. NH TMCP Đông Nam Á (SeABank): In séc
1. NH Liên doanh Việt Nga: In séc, Sổ tiết kiệm
1. CT CP Thiết bị Vật tư Ngân hàng: In séc cho các Ngân hàng
1. Hội đồng XSKT miền Bắc: In Xổ số Kiến thiết miền Bắc
1. CT TNHH MTV XSKT Thủ Đô : In Xổ số Lô tô, Hóa đơn GTGT
1. CT TNHH MTV XSKT Hưng Yên: In Xổ số Lô tô
1. CT TNHH MTV XSKT Ninh Bình: In Xổ số Lô tô
1. CT TNHH MTV XSKT Bắc Giang: In Xổ số Lô tô
1. CT TNHH MTV XSKT Thái Bình: In Xổ số Lô tô
1. CT TNHH MTV XSKT Vĩnh Phúc: In Xổ số Lô tô
1. CT TNHH MTV XSKT Hải Dương: In Xổ số Lô tô
1. CT TNHH MTV XSKT Tuyên Quang: In Xổ số Lô tô
1. CT TNHH MTV XSKT Nam Định: In Xổ số Lô tô
1. CT TNHH MTV XSKT Lạng Sơn: In Xổ số Lô tô
1. CT TNHH MTV XSKT Bắc Ninh: In Xổ số Lô tô
1. CT TNHH MTV XSKT Thanh Hóa: In Xổ số Lô tô
1. CT CP Nhựa Y tế: In nhãn bơm kim tiêm

