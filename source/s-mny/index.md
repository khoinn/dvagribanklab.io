---
title: s_mny
date: 2018-06-21 12:58:09
---

### Các dịch vụ chính:

Thu kiểm đếm, phân loại đóng bó theo tiêu chuẩn;

Điều chuyển, đáp ứng quỹ đến các phòng giao dịch của chi nhánh, nộp và lĩnh tiền tại Ngân hàng Nhà nước. 

Điều chuyển, đáp ứng quỹ đến các phòng giao dịch của chi nhánh, nộp và lĩnh tiền tại Ngân hàng Nhà nước.

Vận chuyển tiền, giấy tờ có giá và tài sản quý; Tiếp quỹ ATM.

### Lợi ích đối với khách hàng sử dụng dịch vụ

Hoạt động dịch vụ ngân quỹ giúp tăng cường tính chuyên môn hoá trong hoạt động ngân hàng.

Đảm bảo tính an toàn trong nghiệp vụ kiểm đếm, vận chuyển tiền và chứng từ có giá.

Góp phần sử dụng hiệu quả các nguồn lực.

Tiết giảm chi phí cho công tác kiểm đếm, khai thác và vận hành máy ATM

Nâng cao tính sẵn sàng phục vụ của hệ thống ATM.


