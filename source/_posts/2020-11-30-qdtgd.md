---
title: Hội nghị Công bố Quyết định về điều động, bổ nhiệm Tổng Giám đốc Công ty TNHH MTV Dịch vụ Agribank.
date: 2020-11-30 15:00:00
categories: "hoạt động nội bộ"
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730572/samples/2020/bonhiemTG%C4%90/bonhiemTGD_preview.jpg  
---

Ngày 30/11/2020, tại Công ty Dịch vụ Agribank diễn ra Lễ công bố Quyết định của Chủ tịch Hội đồng Thành viên Agribank về việc điều động, bổ nhiệm Tổng Giám đốc Công ty TNHH MTV Dịch vụ Agribank.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730572/samples/2020/bonhiemTG%C4%90/bonhiemTGD1.jpg)

*Bà Đỗ Thị Nhàn - Thành viên Hội đồng thành viên phát biểu trong buổi Lễ.*

<p align="justify"> Ông Nguyễn Văn Hà - Thành viên chuyên trách Ủy ban Nhân sự và Tổ chức Đảng đọc Quyết định số 721/QĐ-HĐTV-UBNS ngày 27 tháng 11 năm 2020 của Chủ tịch HĐTV Agribank điều động, bổ nhiệm Tổng Giám đốc Công ty TNHH MTV Dịch vụ Agribank đối với ông Vũ Mạnh Toàn.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730572/samples/2020/bonhiemTG%C4%90/bonhiemTGD2.jpg)

*Ông Nguyễn Văn Hà - Thành viên chuyên trách Ủy ban Nhân sự và Tổ chức Đảng đọc Quyết định điều động, bổ nhiệm.*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730572/samples/2020/bonhiemTG%C4%90/bonhiemTGD3.jpg)

*Bà Đỗ Thị Nhàn - Thành viên Hội đồng thành viên trao Quyết định bổ nhiệm Tổng Giám đốc Công ty Vũ Mạnh Toàn*

<p align="justify"> Tại Lễ công bố, Ông Vũ Mạnh Toàn rất vinh dự và xúc động khi được phát biểu với trọng trách là Tổng Giám đốc Công ty Dịch vụ Agribank, Ông bày tỏ lòng biết ơn và kính trọng đến Hội đồng Thành viên Agribank đã khích lệ, tin tưởng trao nhiệm vụ, đồng thời cũng là thử thách trước những công việc mới.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730572/samples/2020/bonhiemTG%C4%90/bonhiemTGD4.jpg)

*Tổng Giám đốc Vũ Mạnh Toàn phát biểu và nhận nhiệm vụ.*

<p align="justify">Tổng Giám đốc đã phát biểu: "Kính thưa các đồng chí! May mắn lớn nhất của tôi là được kế thừa truyền thống đoàn kết, sáng tạo và những thành tựu đạt được từ các thế hệ lãnh đạo Công ty. Tôi cũng rất mong sẽ tiếp tục nhận được sự ủng hộ, tạo điều kiện, giúp đỡ và chia sẻ của Hội đồng thành viên, Ban điều hành Agribank, Ban lãnh đạo và cán bộ công nhân viên Công ty để tôi có thể hoàn thành tốt nhất nhiệm vụ mới này".
"Chúng ta cần tiếp tục đổi mới, sáng tạo, nâng cao năng lực cạnh tranh và hiệu quả hoạt động, đặc biệt với các thế mạnh trên thị trường về in ấn, dịch vụ ngân quỹ, cho thuê két sắt phát, vận chuyển tiền và tài sản có giá trị cao."<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730588/samples/2020/bonhiemTG%C4%90/bonhiemTGD6.jpg)

*Đảng ủy viên - Phó Tổng Giám đốc Agribank, Chủ tịch Công ty Nguyễn Hải Long phát biểu và giao nhiệm vụ.*

<p align="justify">Phát biểu chỉ đạo tại buổi Lễ, Chủ tịch đã khái quát tình hình thuận lợi, khó khăn của Công ty, đồng thời giao nhiệm vụ cho Tổng Giảm đốc giải quyết dứt điểm tồn tại và định hướng phát triển của Công ty giai đoạn 2021-2026.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730588/samples/2020/bonhiemTG%C4%90/bonhiemTGD5.jpg)
*Đại diện các lãnh đạo chúc mừng Tổng Giám đốc Vũ Mạnh Toàn.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730588/samples/2020/bonhiemTG%C4%90/bonhiemTGD7.jpg)
*Đại diện các đơn vị chúc mừng Tổng Giám đốc Vũ Mạnh Toàn.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1606730588/samples/2020/bonhiemTG%C4%90/bonhiemTGD8.jpg)

*Phó TGĐ Công ty - Phan Việt Trung chúc mừng Tổng Giám đốc Vũ Mạnh Toàn.*

<p align="justify">Tin tưởng vào sự ủng hộ của Đảng ủy, Ban lãnh đạo các cấp và kế thừa truyền thống đoàn kết, sáng tạo của cán bộ, công nhân viên Công ty, mong muốn Tổng Giám đốc sẽ đưa Công ty phát triển bền vững trong giai đoạn 2021-2026.
Buổi Lễ Công bố Quyết định điều động, bổ nhiệm Tổng Giám đốc Công ty TNHH MTV Dịch vụ Ngân hàng Nông nghiệp Việt Nam đã diễn ra tốt đẹp theo đúng chương trình./.<p>