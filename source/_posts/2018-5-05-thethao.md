---
title: Công ty dịch vụ Agribank tham dự giao hữu thể thao, chào mừng Đại hội VI Công đoàn Ngân hàng Việt Nam, Đại hội XII Công đoàn Việt Nam và ngày thành lập Ngân hàng Việt nam 
date: 2018-05-05 15:18:40
categories: "hoạt động nội bộ"
tacgia: Marketing ABSC

preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu1_preview.jpg

---

 Sáng ngày 05 tháng 5 năm 2018 tại Trung tâm TDTT quận Long Biên, Hà Nội, Công ty Dịch vụ Agribank tham dự giao hữu thể thao, chào mừng Đại hội VI Công đoàn Ngân hàng Việt Nam, Đại hội XII Công đoàn Việt Nam và ngày thành lập Ngân hàng Việt nam 


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu5.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288164/samples/2018/giaoluuthethao/giaoluu16.jpg)
