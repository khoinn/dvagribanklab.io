---
title: Lễ trao quà chương trình Chung tay giúp đỡ hộ nghèo khắc phục khó khăn sau lũ tại Vân Đồn
date: 2015-09-11 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304571/samples/tuthien02/tt2_preview.jpg

---

Với ý thức trách nhiệm xã hội của doanh nghiệp trước các vấn đề an sinh xã hội, vì cuộc sống cộng đồng, trong những ngày tháng 9 năm 2015, Công ty Dịch vụ Agribank phối hợp cùng Chi nhánh Agribank tỉnh Bắc Giang và Chi nhánh Agribank tỉnh Quảng Ninh đã tổ chức thăm hỏi, tặng quà cho các hộ gia đình nghèo, bị thiệt hại nặng nề sau lũ, góp phần chia sẻ khó khăn, giúp bà con sớm ổn định cuộc sống.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304567/samples/tuthien02/t21.jpg)

*Đoàn đại diện Công ty Dịch vụ Agribank do ông Đỗ Đình Hồng chủ tịch HĐTV cùng đại diện Công đoàn, đoàn thanh niên tới trao quà cho bà con tại Vân đồn*

Trận mưa lũ lớn nhất trong 50 năm ở Quảng Ninh cuối tháng 7/2015 đã gây thiệt hại lớn, trong đó Vân Đồn là một trong những huyện chịu ảnh hưởng nặng nề khiến cuộc sống của bà con nơi đây càng thêm khó khăn. Hưởng ứng lời kêu gọi của UBTW Mặt trận Tổ quốc Việt Nam, phát huy tinh thần tương thân tương ái, Công ty Dịch vụ Agribank cùng Chi nhánh Agribank tỉnh Quảng Ninh đã vận động cán bộ công nhân viên quyên góp gạo, tiền và đến tận nơi thăm hỏi, trao 35 suất quà cho các hộ nghèo tại xã đảo Vạn Yên, huyện Vân Đồn. Trong chuyến đi này, về phía Công ty có đồng chí Đỗ Đình Hồng - Bí thư Đảng ủy, Chủ tịch HĐTV cùng đại diện Công đoàn và Đoàn Thanh niên.

Tại Bắc Giang, đoàn công tác của Công ty Dịch vụ Agribank và Chi nhánh Agribank tỉnh Bắc Giang đã trực tiếp đến xã Đồng Tiến, huyện Yên Thế kịp thời động viên, trao tặng 01 tấn gạo và nhiều phần quà bằng tiền mặt ý nghĩa tới tận tay các gia đình nghèo, góp một phần khắc phục khó khăn sau mưa to, lũ quét phải di dời 67 hộ dân cuối tháng 7, đầu tháng 8/2015.

Lãnh đạo cùng nhân dân các địa phương đã bày tỏ sự cảm kích, trân trọng ghi nhận tình cảm và nghĩa cử đầy trách nhiệm của Công ty Dịch vụ Agribank, Chi nhánh Agribank tỉnh Quảng Ninh và Bắc Giang. 

Bên cạnh nỗ lực ổn định, phát triển sản xuất kinh doanh, Công ty Dịch vụ Agribank sẽ tiếp tục phối hợp với các đơn vị, chi nhánh trong hệ thống tích cực chung tay cùng cộng đồng trong các hoạt động an sinh xã hội, góp phần nâng cao hình ảnh, uy tín thương hiệu và lan tỏa bản sắc văn hóa nghĩa tình tốt đẹp của Agribank.

 Ông Đỗ Đình Hồng chủ tịch HĐTV cùng đại diện Công đoàn, đoàn thanh niên, và Chi nhánh Agribank Quảng ninh trao quà cho các hộ nghèo tại xã đảo Vạn Yên, huyện Vân Đồn

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304570/samples/tuthien02/tt2_0.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304568/samples/tuthien02/t22.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304568/samples/tuthien02/t23.jpg)

*Niềm vui của bà con khi nhận được những phần quà đầy tình nghĩa*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304568/samples/tuthien02/t24.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304569/samples/tuthien02/t25.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304569/samples/tuthien02/t26.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304570/samples/tuthien02/t27.jpg)


 Lễ trao quà chương trình chung tay giúp đỡ hộ nghèo, khắc phục khó khăn sau lũ tại xã Đồng Tiến, huyện Yên Thế, tỉnh Bắc Giang

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304566/samples/tuthien02/bg1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304566/samples/tuthien02/bg2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304567/samples/tuthien02/bg3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304567/samples/tuthien02/bg4.jpg)