---
title: Thống đốc Ngân hàng Nhà nước thăm và chúc mừng Agribank ngày làm việc đầu tiên của năm mới 2017
category: hoạt động ngành
date: 2017-02-02 
tacgia: Nguồn agribank.com.vn
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304140/samples/2017/thongdoc/thongdoc_preview.jpg

---

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304140/samples/2017/thongdoc/thongdoc.jpg)

Trong không khí phấn khởi của ngày làm việc đầu tiên của năm mới Đinh Dậu 2017, sáng ngày 02/02/2017 (tức ngày mùng 6 Tết), Agribank vinh dự được đón đồng chí Lê Minh Hưng, Ủy viên BCH Trung ương Đảng, Bí thư Ban Cán sự Đảng, Thống đốc Ngân hàng Nhà nước Việt Nam đến thăm và chúc mừng. Đây là sự động viên khích lệ đối với tập thể Hội đồng Thành viên, Ban Lãnh đạo và 40.000 cán bộ, viên chức Agribank khi bước sang năm 2017 với nhiều nỗ lực quyết tâm phấn đấu hoàn thành mọi nhiệm vụ, mục tiêu đề ra, tiếp tục khẳng định vai trò “Lá cờ đầu”, Ngân hàng giữ vai trò then chốt trên thị trường tín dụng “Tam nông”.




