---
title: CÔNG TY DỊCH VỤ AGRIBANK CHÚC MỪNG KỶ NIỆM 1 NĂM NGÀY THÀNH LẬP TRUNG TÂM NGÂN QUỸ MIỀN BẮC
categories: hoạt động nội bộ
date: 2022-11-19
tacgia: Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668849881/samples/2022/chucmungNQMB/nganquy1.jpg

---

<p align="justify">Nhân dịp kỷ niệm tròn 1 năm ngày thành lập Trung tâm Ngân quỹ Miền Bắc, Ban lãnh đạo Công ty Dịch vụ Agribank đã đến chúc mừng và có lẵng hoa tươi thắm tặng Ban lãnh đạo và các cán bộ, công nhân viên tại đơn vị.
Đại diện Công ty Dịch vụ Agribank có đồng chí Trần Duy Hưng - Chủ tịch Công ty, đồng chí Trịnh Ngọc Tùng - Phó Tổng Giám đốc, đồng chí Trần Thu Thủy - Phó Tổng Giám đốc Công ty.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668849881/samples/2022/chucmungNQMB/nganquy1.jpg)

Tại buổi gặp gỡ, đồng chí Trần Duy Hưng vui mừng chúc Trung tâm Ngân quỹ Miền Bắc ngày càng phát triển, hoàn thành xuất sắc sứ mệnh và mục tiêu của đơn vị. Đồng chí cũng bày tỏ sự tin tưởng vào sự hợp tác bền vững, lâu dài của hai bên, đảm bảo an toàn, an ninh cho hoạt động ngân quỹ, đóng góp vào sự nghiệp phát triển chung của Agribank.




