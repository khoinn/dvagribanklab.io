---
title: CÔNG TY DỊCH VỤ AGRIBANK GIAO LƯU BÓNG ĐÁ GIẢI TỨ HÙNG
Date: 2022-6-11
categories: hoạt động nội bộ
tacgia: Kinh doanh Tiếp thị ASC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655107131/samples/2022/bongdatuhung/tuhung_preview.jpg

---
 Sáng ngày 11/6/2022, tại sân bóng Song Minh, Thành phố Hạ Long, Tỉnh Quảng Ninh, Công ty Dịch vụ Agribank tham gia giao lưu bóng đá Giải Tứ hùng do Agribank Chi nhánh Tỉnh Quảng Ninh tổ chức.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung2.jpg)

Tham dự giải có 04 đội bóng của Agribank Chi nhánh Tỉnh Quảng Ninh, Công ty Dịch vụ Agribank, Agribank Chi nhánh Hà Tây I và Agribank Chi nhánh Bắc Hải Phòng.
Đến dự động viên tinh thần các cầu thủ có đồng chí Nguyễn Hải Long- Phó Tổng Giám đốc Agribank, các đồng chí lãnh đạo Chi nhánh, Công ty Dịch vụ Agribank cùng cán bộ, nhân viên của bốn đơn vị cổ vũ cho buổi giao lưu. 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung5.jpg)

Trận chung kết giữa Agribank Chi nhánh Tỉnh Quảng Ninh và Công ty Dịch vụ Agribank diễn ra quyết liệt và gay cấn với nhiều pha ghi bàn đẹp mắt. Kết quả chung cuộc, đội chủ nhà Agribank Chi nhánh Tỉnh Quảng Ninh đã có chiến thắng 2-1 trước Công ty Dịch vụ Agribank và giành vị trí nhất giải.

Các chương trình giao lưu thể thao của các đơn vị trong hệ thống Agribank nhằm rèn luyện, nâng cao sức khoẻ cho cán bộ, người lao động, tạo không khí vui tươi phấn khởi, tạo động lực nâng cao năng suất lao động, đồng thời giúp tăng tình đoàn kết, giao lưu, trao đổi những kinh nghiệm trong hoạt động kinh doanh của các đơn vị trong hệ thống Agribank và là bước tiền khởi động cho Hội thao Agribank năm 2022./.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung7.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung6.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung8.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung9.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung10.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung11.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655106570/samples/2022/bongdatuhung/tuhung12.jpg)







