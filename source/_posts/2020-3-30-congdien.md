---
title: Chỉ thị 15/CT-TTg 27/3/2020 của Thủ tướng Chính phủ & Công điện khẩn 01/CĐ-UBND 28/3/2020 của Chủ tịch UBND Thành phố Hà Nội
date: 2020-3-30
categories: hoạt động nội bộ
tacgia: Trích dẫn Cổng thông tin điện tử chinhphu.vn
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1585539360/samples/2020/congdien/ct15_preview.jpg 
---

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1585539361/samples/2020/congdien/ct15_1.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1585539361/samples/2020/congdien/ct15_2.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1585539361/samples/2020/congdien/ct15_3.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1585539361/samples/2020/congdien/CD-012020_1.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1585539361/samples/2020/congdien/CD-012020_2.jpg)
