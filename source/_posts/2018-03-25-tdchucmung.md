---
title: Thư chúc mừng của Thống đốc NHNN nhân dịp kỷ niệm 30 năm thành lập Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam
date: 2018-03-25 
categories: "hoạt động ngành"
tacgia: agribank.com.vn
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287910/samples/2018/thongdocchuctet/thongdoc_preview.jpg
---

Nhân dịp kỷ niệm 30 năm ngày thành lập Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam (26/3/1988- 26/3/2018), thay mặt Ban cán sự Đảng, Ban Lãnh đạo Ngân hàng Nhà nước Việt Nam, tôi thân ái gửi đến các thế hệ cán bộ lãnh đạo và người lao động của Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam (Agribank) lời chúc mừng tốt đẹp nhất.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287910/samples/2018/thongdocchuctet/thongdoc.jpg)  
*Thống đốc Ngân hàng Nhà nước Lê Minh Hưng*

Trải qua 30 năm xây dựng và phát triển, Agribank đã khẳng định được vị thế, vai trò của một trong những Ngân hàng Thương mại hàng đầu Việt Nam, luôn đồng hành cùng sự nghiệp phát triển nông nghiệp, nông dân, nông thôn, có nhiều đóng góp tích cực thúc đẩy quá trình tái cơ cấu nền kinh tế, xây dựng nông thôn mới và bảo đảm an sinh xã hội. Tôi biểu dương tinh thần đoàn kết, ý chí quyết tâm, nỗ lực của các thế hệ cán bộ lãnh đạo, người lao động Agribank; ghi nhận và đánh giá cao những kết quả và đóng góp của Agribank đối với sự nghiệp phát triển đất nước nói chung và ngành Ngân hàng nói riêng

Bước sang giai đoạn phát triển mới, nhiệm vụ đặt ra đối với Agribank là tiếp tục bám sát các chủ trương, chính sách của Đảng, Chính phủ và giải pháp điều hành của Ngân hàng Nhà nước, tập trung triển khai có hiệu quả Chiến lược kinh doanh giai đoạn 2016- 2020, tầm nhìn 2030, thực hiện thành công tái cơ cấu giai đoạn 2 gắn với nhiệm vụ đẩy nhanh tiến trình thực hiện kế hoạch cổ phần hóa Agribank theo Quyết định của Thủ tướng Chính phủ. Tôi tin tưởng rằng, với truyền thống 30 năm xây dựng và phát triển, tập thể Ban Lãnh đạo cùng toàn thể cán bộ, người lao động Agribank sẽ tiếp tục đoàn kết, năng động, sáng tạo, phấn đấu hoàn thành tốt mọi nhiệm vụ được giao, giữ vững vị trí, vai trò của Agribank trên thị trường tài chính nông nghiệp, nông thôn, tiếp tục đóng góp tích cực vào kết quả chung của ngành Ngân hàng, góp phần thúc đẩy phát triển kinh tế - xã hội

Chúc toàn thể cán bộ, nhân viên Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam mạnh khỏe, hạnh phúc và thành công!
  
![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287910/samples/2018/thongdocchuctet/chuky2.jpg)
