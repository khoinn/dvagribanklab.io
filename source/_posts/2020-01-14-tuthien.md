---
title: Công ty Dich vụ Agribank thăm và tặng quà người có hoàn cảnh khó khăn nhân dịp tết nguyên đán Canh tý 2020
Date: 2020-01-14 9:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974964/samples/2019/tuthien20/tuthien20_preview.jpg 
---

<p align="justify"> Trong những ngày chuẩn bị đón Xuân Canh tý 2020, Đ/c Đỗ Đình Hồng, Chủ tịch - Tổng Giám đốc Công ty cùng  Đoàn Thanh niên Công ty Dịch vụ Agribank  đến thăm hỏi và tặng quà Tết cho các cháu tại Làng trẻ SOS Hòa Bình quận Thanh Xuân - Hà Nội một tấn gạo. Làng trẻ Hòa Bình là nơi phục hồi chức năng chăm sóc, nuôi dưỡng, hỗ trợ trẻ em có hoàn cảnh đặc biệt khó khăn. Cũng ngay trong sáng nay, đoàn từ thiện tiếp tục đến với UBND phường Thanh Xuân Trung, trao 33 suất quà tổng trị giá 10 triệu đồng cho các gia đình có hoàn cảnh khó khăn trên địa bàn phường.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_5.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_6.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_7.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1578974962/samples/2019/tuthien20/tuthien20_8.jpg)

<p align="justify"> Sáng 15 tháng 01 đoàn đến thăm và trao tặng một tấn gạo tại Trung tâm Bảo trợ Xã hội 2, huyện Ứng Hòa, Hà Nội- nơi chăm sóc 240 người già neo đơn khó khăn, cơ nhỡ, người bệnh tật nguyền."Vậy là Tết này tiền gạo chuyển sang mua thịt, cho những bữa ăn ngày Tết thêm ngon" - Giám đốc Trung tâm chia sẻ trong lời cảm ơn tới tấm lòng vàng của Công ty Dịch vụ Agribank.</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1579062947/samples/2019/tuthien20/8.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1579062948/samples/2019/tuthien20/7.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1579062947/samples/2019/tuthien20/5.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1579062947/samples/2019/tuthien20/9.jpg)


<p align="justify"> Các chương trình thăm hỏi, phát quà được tổ chức thường xuyên của Công ty Dịch vụ Agribank với sự quan tâm của Đảng ủy, Lãnh đạo Công ty cùng toàn thể cán bộ công nhân viên, sự tích cực của các Đoàn viên thanh niên trên tinh thần tự nguyện, quan tâm tới công tác từ thiện và an sinh xã hội góp phần nhỏ bé giúp vơi đi những khó khăn của cộng đồng </p>