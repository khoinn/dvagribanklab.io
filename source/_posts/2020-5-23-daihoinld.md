---
title: Hội nghị đại biểu người lao động Công ty Dịch vụ Agribank năm 2020
categories: hoạt động nội bộ
date: 2020-5-23
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld_preview.jpg
---
Ngày 23 tháng 5 năm 2020 Công ty TNHH MTV Dịch vụ Ngân hàng Nông nghiệp Việt Nam (Công ty Dịch vụ Agribank) đã tổ chức thành công Hội nghị Người Lao động năm 2020.

Hội nghị với 100% đại biểu nhất trí tán thành với bản Thỏa ước lao động tập thể hiện hành có giá trị tới năm 2021 và đã bầu ra Tổ đối thoại và Ban thanh tra Nhân dân.

Một số hình ảnh trong Hội nghị Đại biểu Người lao động Công ty Dịch vụ Agribank năm 2020.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld3.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld4.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld5.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld6.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld7.jpg)



![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld8.jpg)



![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1590208545/samples/2020/daihoinld2020/daihoinld9.jpg)
