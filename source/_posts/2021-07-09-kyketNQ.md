---
title: Lễ ký kết Phụ lục Hợp đồng sửa đổi, bổ sung hợp đồng nguyên tắc Dịch vụ Ngân quỹ giữa Agribank và Công ty dịch vụ Agribank
Date: 2021-07-09 15:10:35
categories: hoạt động nội bộ
tacgia: Phòng Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1625818173/samples/2021/kyketnq21/kyket21_preview.jpg 

---

<p align="justify"> Sáng ngày 09/7/2021, tại Hội trường Trụ sở chính Agribank, số 2 Láng Hạ, Ba Đình, Hà Nội đã diễn ra Lễ ký kết Phụ lục Hợp đồng sửa đổi, bổ sung Hợp đồng nguyên tắc dịch vụ ngân quỹ số 01/2016/HĐNT-DVNQ ngày 21/4/2016 giữa Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam (Agribank) và Công ty TNHH MTV Dịch vụ Ngân hàng Nông nghiệp Việt Nam (Công ty Dịch vụ Agribank).</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1625818174/samples/2021/kyketnq21/kyket21_1.jpg) 

*Ông Nguyễn Minh Phương, Phó Tổng giám đốc Agribank (bên phải) và Ông Nguyễn Hải Long, Phó Tổng giám đốc Agribank- Chủ tịch  Công ty Dịch vụ Agribank (bên trái) trong Lễ ký kết*

<p align="justify"> Tham dự Lễ ký kết, về phía Ngân hàng Nông nghiệp và Phát triển Nông thôn có Ông Nguyễn Minh Phương, Phó Tổng giám đốc Agribank cùng đại diện các Ban Tiền tệ Kho quỹ, Pháp chế và Kiểm soát tuân thủ, Tài chính Kế toán, Đầu tư. Về phía Công ty Dịch vụ Agribank có Ông Nguyễn Hải Long, Phó Tổng giám đốc Agribank - Chủ tịch Công ty Dịch vụ Agribank, đại diện lãnh đạo các phòng ban và Giám đốc Trung tâm Dịch vụ Ngân quỹ Miền Bắc.</p>

<p align="justify">Một trong những nội dung quan trọng trong Phụ lục Hợp đồng sửa đổi, bổ sung Hợp đồng nguyên tắc dịch vụ ngân quỹ lần này là: mở rộng phạm vi cung ứng dịch vụ ngân quỹ ra địa bàn lân cận khu vực TP. Hà Nội và TP. Hồ Chí Minh để đáp ứng nhu cầu sử dụng dịch vụ ngân quỹ của các chi nhánh, góp phần tăng hiệu quả sử dụng vốn của toàn hệ thống. Việc mở rộng địa bàn có thể tận dụng được nguồn lực sẵn có của hai Trung tâm dịch vụ ngân quỹ mà không làm phát sinh đầu tư thêm cơ sở vật chất, phương tiện, con người tại hai Trung tâm; không làm ảnh hưởng đến quy trình, quy định hiện hành của Agribank về hoạt động ngân quỹ nói chung và dịch vụ ngân quỹ nói riêng.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1625818174/samples/2021/kyketnq21/kyket21_2.jpg)

<p align="justify">Việc cho phép mở rộng địa bàn dịch vụ ngân quỹ một lần nữa khẳng định sự quan tâm, tin tưởng của Ban Lãnh đạo Agribank đối với Công ty Dịch vụ Agribank trong việc triển khai các phương án sản xuất kinh doanh, giúp hai Trung tâm DVNQ tăng hiệu quả hoạt động, giảm bớt khó khăn, ổn định công ăn việc làm và tăng thu nhập cho người lao động; từng bước cải thiện tình hình tài chính, cân bằng thu, chi và có lợi nhuận.</p>

<p align="justify">Buổi lễ ký kết đã thành công tốt đẹp, các nội dung ký kết sẽ sớm được Agribank và Công ty Dịch vụ đưa vào thực hiện đầy đủ, nghiêm túc, triển khai có hiệu quả trong thời gian tới. </p>




