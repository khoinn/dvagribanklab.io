---
title: Đại Hội Công đoàn cơ sở Công ty Dịch vụ Agribank
date: 2017-12-11 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304037/samples/2017/daihoicongdoan/cd_preview.jpg

---
Ngày 04 - 11- 2017 ( Thứ 7). Tại Hội trường Công ty Dịch vụ Agribank, số 10 Chùa Bộc, Đống Đa, Hà Nội. Được sự đồng ý của Đảng ủy Công ty và sự chấp thuận kế hoạch Đại hội của thường vụ Công đoàn Agribank, Công đoàn Công ty đã tổ chức Đại hội Đại biểu Công đoàn cơ sở nhiệm kỳ 2017 – 2022. Đến dự về phía Công ty có Đồng chí Đỗ Đình Hồng: Bí thư Đảng ủy, Chủ tịch kiêm Tổng Giám Đốc Công ty. Đại biểu Công đoàn cấp trên: Đồng chí Ngô Anh Tuấn, Phó Chủ tịch Công đoàn Agribank. Đồng chí Đặng Văn Chúc, Chủ nhiệm Ủy ban kiểm tra Công đoàn Agribank. Thành phần tham gia Đại hội gồm đồng chí Lê Minh Quang Chủ tịch Công đoàn Công ty- Phó Tổng Giám Đốc Công ty và gần 80 Đại biểu đại diện cho Công đoàn các bộ phận trong Công ty.

Khai mạc Đại hội bắt đầu bằng một trương trình văn nghệ của đội văn nghệ Công ty Dịch vụ Agribank, được thể hiện sôi động và trình diễn rất chuyên nghiệp. Tiếp theo Đồng chí Phạm Quang Huy đọc báo cáo tổng kết công tác Công đoàn nhiệm kỳ 2012- 2017, và phương hướng nhiệm kỳ 2017 – 2022. Công ty Dịch vụ Agribank là một công ty độc lập trực thuộc Ngân hàng nông nghiệp và phát triển nông thôn Việt Nam, với số lượng đoàn viên là 382 đoàn viên được tổ chức thành 5 đơn vị trong đó có 2 Công đoàn CSTV phía Nam ( được sáp nhập vào Công đoàn Công ty từ năm 2014) và 3 Công đoàn bộ phận ở Hà Nội phù hợp với tổ chức bộ máy và các hoạt động đa dạng của Công ty. Tổ chức Công đoàn với chức năng là bảo vệ quyền và lợi ích hợp phát của người lao động trong doanh nghiệp, tham gia cùng chuyên môn quản lý, giáo dục động viên người lao động hăng say làm việc, làm việc có năng suất, chất lượng, an toàn và hiệu quả.

Trong những năm qua Công ty đã trải qua những thuận lợi, khó khăn nhưng tập thể CBCNV dưới sự lãnh đạo của Đảng ủy và ban Lãnh đạo Công ty đã đoàn kết nhất trí hăng say lao động sáng tạo không ngừng quyết tâm hoàn thành tốt các kế hoạch SX- KD đã đề ra. Tuy nhiên vẫn còn tồn tại khuyết điểm cần phải khắc phục trong nhiệm kỳ mới. 
Đại hội đã đề ra phương hướng nhiệm vụ nhiệm kỳ 2017 – 2022.
Đại hội tổ chức bầu ra ban chấp hành mới gồm:
-	Ông Lê Minh Quang - Chủ tịch Công đoàn Công ty.
-	Ông Phạm Quang Huy -  Phó Chủ tịch thường trực Công đoàn Công ty.
-	Ông Trần Việt Hùng -  Phó Chủ tịch Công đoàn Công ty.
-	Ông Nguyễn Việt Cường -  Ủy viên ban chấp hành Công đoàn Công ty.
-	Bà Dương Lệ Hằng - Ủy viên ban chấp hành Công đoàn Công ty.
-	Ông Phan Việt Trung -  Ủy viên ban chấp hành Công đoàn Công ty.
-	Ông Nguyễn Thế Minh - Ủy viên ban chấp hành Công đoàn Công ty.

Đồng chí Đỗ Đình Hồng đã có những ý kiến chỉ đạo của Đảng về công tác tư tưởng cần nắm bắt kịp thời để bảo vệ quyền lợi cho người lao động. Đồng chí đã nêu ra những thuận lợi,  khó khăn trong những năm tới và yêu cầu tất cả đoàn viên Công đoàn, người lao động phải đoàn kết để cùng với ban Lãnh đạo vượt qua những khó khăn, giữ vững sự ổn định và phát triển Công ty ngày càng lớn mạnh.


Đồng chí Ngô Anh Tuấn đại diện cho Công đoàn Agribank có ý kiến chỉ đạo: Mong muốn các đồng chí trong tổ chức Công đoàn Công ty tham gia thực hiện tốt các nhiệm vụ kinh doanh, đoàn kết nội bộ để ổn định và phát triển Công ty “ Nếu biết vì nhau, vì tập thể thì cuộc sống trở thành Thiên Đường”

Đồng chí Lê Minh Quang, Chủ tịch Công đoàn Công ty đại diện cho toàn thể đoàn viên Công đoàn Công ty cảm ơn, tiếp thu ý kiến chỉ đạo của Đảng Ủy Công ty và Công đoàn Agribank.


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd1.jpg)
*Đồng chí Đỗ Đình Hồng phát biểu tại Đại hội*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd2.jpg)
*Đồng chí Ngô Anh Tuấn phát biểu tại Đại hội*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd3.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd4.jpg)
*Đồng chí Lê Minh Quang phát biểu tại Đại hội*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd5.jpg)
*Đồng chí Phạm Quang Huy đọc báo cáo tại Đại hội*
 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd6.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd7.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd8.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd9.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd10.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd11.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd12.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd13.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304027/samples/2017/daihoicongdoan/cd14.jpg)