---
title: Công ty Dịch vụ Agribank chung tay xoa dịu nỗi đau da cam
date: 2018-08-08 15:18:40
categories: "hoạt động nội bộ"
tacgia: Phòng Marketing
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288196/samples/2018/dacam/dacam_preview.jpg

---

 ![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288196/samples/2018/dacam/dacam1.jpg)

Ngày 08/8/2018, nhân dịp kỉ niệm ngày “ Vì nạn nhân chất độc da cam / dioxin Việt Nam” 10/8/1961 – 10/8/2018 và vinh danh các bà mẹ nuôi chồng con bị nhiễm chất độc da cam / dioxin. Công ty Dịch vụ Agribank đã tổ chức một đoàn đại diện cho CBCNV Công ty đến tặng quà cho các nạn nhân chất độc da cam tại Quận Thanh Xuân. 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288196/samples/2018/dacam/dacam2.jpg)

Dẫn đầu là đồng chí Đỗ Đình Hồng – Chủ tịch kiêm Tổng Giám đốc Công ty Dịch vụ Agribank cùng Ban lãnh đạo, nòng cốt là các đoàn viên thanh niên đã đến dự mit ting để tưởng nhớ những nạn nhân nhiễm chất độc da cam đã qua đời. 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288197/samples/2018/dacam/dacam3.jpg)

Tại buổi lễ, Hội nạn nhân chất độc da cam Quận Thanh Xuân biểu dương các cán bộ hội viên đã hoàn thành xuất sắc nhiệm vụ trong thời gian qua, các bà mẹ chăm sóc chồng con bị nhiễm chất độc da cam đồng thời ghi nhớ cảm ơn các tổ chức cá nhân hết sức giúp đỡ về tinh thần vật chất cho các nạn nhân để họ có nghị lực vươn lên hòa nhập cuộc sống cộng đồng. Đoàn Công ty Dịch vụ Agribank đã tặng 20 suất  quà cho các nạn nhân chất độc da cam Quận Thanh Xuân. 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288197/samples/2018/dacam/dacam4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288197/samples/2018/dacam/dacam5.jpg)


Đây là một nghĩa cử cao đẹp, là hoạt động thường niên của Công ty Dịch vụ Agribank luôn dang rộng vòng tay giúp đỡ, chia sẻ với truyền thống và văn hóa Agribank. 


  
