---
title: Thông điệp của Chủ tịch Hội đồng Thành viên nhân dịp kỷ niệm 30 năm thành lập Agribank
date: 2018-03-24 
categories: "hoạt động ngành"
tacgia: agribank.com.vn
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288676/samples/2018/thongdiep_preview.jpg
---

2018, đánh dấu chặng đường 30 năm Agribank lớn lên và đồng hành cùng nền kinh tế đất nước, không ít gian nan, thách thức tưởng chừng như khó thể vượt qua. Từ bờ vực của sự phá sản trong ngày đầu chuyển sang kinh tế thị trường đến những tổn thất và mất mát do hậu quả của thời kỳ tăng trưởng nóng, sự chuyển dịch thiếu chuẩn bị từ khu vực nông thôn về hai thành phố lớn. 

Nhưng “lửa thử vàng, gian nan thử sức”, chúng ta đã cùng nhau đoàn kết, chung sức, đồng lòng quyết xây dựng một Agribank kỷ cương, thượng tôn pháp luật; Luôn là một trong 04 ngân hàng hàng đầu Việt Nam và là ngân hàng lớn nhất trong nhiều lĩnh vực như tổng tài sản, nguồn vốn, dư nợ, đặc biệt dư nợ cho vay nông nghiệp nông thôn, số lượng khách hàng, hệ thống mạng lưới, số lượng nhân viên có tay nghề cao… đứng thứ 446/1000 ngân hàng lớn nhất thế giới.

Nhân dịp này, thay mặt Ban lãnh đạo Agribank, tôi xin chân thành cảm ơn sự quan tâm lãnh đạo, chỉ đạo của Đảng, Nhà nước, Quốc hội, Chính phủ; các Bộ, Ban, ngành Trung ương; Cảm ơn cấp ủy, chính quyền địa phương cả nước; Cảm ơn sự hợp tác thủy chung của hàng chục triệu bạn hàng, đối tác trong và ngoài nước. Cảm ơn sự nỗ lực vượt khó vươn lên hoàn thành nhiệm vụ của các thế hệ người lao động Agribank - Những nhân tố quyết định thành công của Agribank trong suốt 30 năm qua.

Hơn 40.000 người lao động Agribank yêu quý!

Tự hào về Agribank, tự hào về những gì chúng ta đã đạt được, nhưng cũng phải thừa nhận rằng chúng ta đang thực hiện sứ mệnh lịch sử trong một lĩnh vực có rủi ro cao do “thiên tai bất khả kháng”, do “được mùa mất giá”, do “cho vay món nhỏ chi phí cao” và lãi suất chúng ta cho vay thấp nên chênh lệch thu chi không nhiều. Trong khi đó cán bộ chúng ta đông nhưng năng suất lao động chưa cao, hệ thống mạng lưới của chúng ta rộng nhưng chưa thực sự thành lợi thế do khó tin học hóa v,v.... đó là những thách thức không hề nhỏ trong lộ trình tái cơ cấu giai đoạn 2 chuyển sang hoạt động theo mô hình tổ chức tín dụng cổ phần do Nhà nước nắm cổ phần chi phối. Nhận biết được những thách thức này không phải để chùn bước mà để chúng ta phải biết yêu ngôi nhà Agribank của chúng ta hơn, quyết tâm cùng nhau nâng niu nó, giữ gìn nó và đổi mới nó.

Tôi tin tưởng sâu sắc rằng, với truyền thống và thành tựu vẻ vang của 30 năm xây dựng và phát triển, Agribank tiếp tục nắm vững cơ hội, sáng tạo để vượt qua thử thách, tiếp tục phát triển ổn định, bền vững, có tầm ảnh hưởng trong nước và quốc tế.

Chúng ta hoàn toàn có quyền hy vọng và tin tưởng, với sự quan tâm của cả hệ thống chính trị cho “Tam nông” như hiện nay, hiểu Agribank và vận hành Ngân hàng này một cách bài bản, chắc chắn tương lai của Agribank là vô cùng tươi sáng.

Xin gửi đến toàn thể anh chị em đã và đang công tác trong toàn hệ thống Agribank cùng gia đình lời chúc mừng tốt đẹp nhất./.

TM. HỘI ĐỒNG THÀNH VIÊN

**Chủ tịch**  
**Trịnh Ngọc Khánh **