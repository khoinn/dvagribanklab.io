---
title: Lễ công bố quyết định kéo dài thời hạn giữ chức vụ Chủ tịch - Tổng Giám đốc Công ty Dịch vụ Agribank
date: 2018-06-04 15:18:40
categories: "hoạt động nội bộ"
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287927/samples/2018/taibonhiem/taibonhiem_preview.jpg
---

Ngày 04/6/2018, tại hội trường Công ty Dịch vụ Agribank diễn ra Lễ công bố quyết định của Chủ tịch Hội đồng Thành viên Agribank về việc kéo dài thời hạn giữ chức vụ Chủ tịch kiêm Tổng Giám đốc Công ty Dịch vụ Agribank đối với ông Đỗ Đình Hồng.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem1.jpg)
*Ông Phạm Đức Ấn - Phó Chủ tịch  Hội đồng thành viên Agribank
công bố quyết định kéo dài thời hạn giữ chức vụ của ông Đỗ Đình Hồng*


Phát biểu tại buổi lễ, Ông Phạm Đức Ấn - Phó Chủ tịch  Hội đồng thành viên Agribank chúc mừng ông Đỗ Đình Hồng. Ông đã ghi nhận những thành tích trong thời gian vừa qua và chỉ ra những khó khăn thách thức đối với Công ty trong giai đoạn tới, tuy nhiên ông cũng bày tỏ niềm tin tưởng vào sự lãnh đạo của Ban Tổng Giám đốc, sự đoàn kết, gắn bó của tập thể cán bộ, nhân viên Công ty Dịch vụ Agribank sẽ vượt qua những khó khăn, giữ vững và phát huy những thành tích đã đạt được, hoàn thành nhiệm vụ quan trọng của Ngân hàng Nhà nước tin tưởng giao phó.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem2.jpg)
*Ông Phạm Đức Ấn - Phó Chủ tịch  Hội đồng thành viên Agribank chúc mừng và giao nhiệm vụ*

Tại buổi lễ, ông Đỗ Đình Hồng cảm ơn sự quan tâm, tin tưởng của Hội đồng thành viên Agribank và mong muốn Agribank tạo điều kiện thuận lợi để công ty mở rộng phát triển, nâng cao năng lực cạnh tranh và hứa quyết tâm cùng Ban Tổng Giám đốc, toàn thể cán bộ công nhân viên Công ty tiếp tục phát huy những thành tích đã đạt được vào sự phát triển bền vững của Công ty cùng  hệ thống Agribank.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem3.jpg)
*Ông Đỗ Đình Hồng đã nhận nhiệm vụ và hứa cố gắng hơn nữa trong thời gian tới*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem4.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem5.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem6.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem7.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem8.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem9.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem10.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543287928/samples/2018/taibonhiem/taibonhiem11.jpg)









  







