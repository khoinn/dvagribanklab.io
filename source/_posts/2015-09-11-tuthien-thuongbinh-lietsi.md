---
title: Công ty Dịch vụ Agribank tổ chức các hoạt động hướng tới ngày kỷ niệm thương binh liệt sỹ 27-7
date: 2015-09-11 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304547/samples/tuthien01/tt_preview.jpg

---

Phát huy truyền thống uống nước nhớ nguồn, Công ty tích cực tổ chức các hoạt động hướng tới ngày kỷ niệm thương binh liệt sỹ 27/7 nhằm ghi nhớ và tôn vinh công lao to lớn của các anh hùng liệt sỹ, thương binh, bệnh binh, gia đình có công với nước trong cuộc kháng chiến giải phóng dân tộc và bảo vệ tổ quốc, từ đó phát huy truyền thống “uống nước nhớ nguồn”, “đền ơn đáp nghĩa” trong cán bộ công nhân viên toàn Công ty như chương trình thăm hỏi, tặng quà gia đình Liệt sỹ, Thương binh trên địa bàn Công ty có trụ sở tại Phường Quang Trung, Quận Đống Đa, Thành phố Hà Nội


Ông Đỗ Đình Hồng chủ tịch HĐTV Công ty Dịch vụ Agribank thăm hỏi và tặng quà các gia đình nhân ngày thương binh liệt sỹ 27-7 tại Phường Quang Trung Q. Đóng Đa Hà Nội.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304546/samples/tuthien01/tt1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304546/samples/tuthien01/tt2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304546/samples/tuthien01/tt3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304546/samples/tuthien01/tt4.jpg)