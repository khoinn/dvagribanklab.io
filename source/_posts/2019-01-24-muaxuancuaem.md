---
title: Công ty Dịch vụ Agribank tham gia tài trợ Chương trình "Mùa xuân của em" lần thứ II-2019 với chủ đề "Tết ấm - Học sinh vùng cao" do Hội Nhà báo Việt Nam tổ chức vào ngày 24/01/2019 tại Nhà hát Lớn Tp. Hà Nội và truyền hình trực tiếp trên sóng VTC1
Date: 2019-01-24 8:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1548303921/samples/2019/muaxuancuaem/muaxuancuaem_preview.jpg

---

<p align="justify">Trong không khí đón chào một mùa xuân mới Kỷ Hợi 2019, nhân tháng hành động vì người nghèo, thực hiện Chỉ thị số 20-CT/TW của Bộ Chính trị, nhằm mang lại một mùa xuân ấm áp nghĩa tình cho các em nhỏ vùng cao chịu nhiều khó khăn do bão lũ, Hội Nhà báo Việt Nam phối hợp với các đơn vị cùng sự hảo tâm của các nhà Tài trợ, tổ chức  Chương trình "Mùa xuân của em" lần thứ II-2019 với chủ đề "Tết ấm-Học sinh vùng cao". Đối tượng nhận quà Tết là các em học sinh vùng cao vượt khó học giỏi, tài trợ việc sửa chữa xây dựng trường học bị thiệt hại do mưa lũ tại các tỉnh Điện Biên, Lai Châu, Hà Giang, Yên Bái, Lào Cai.</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1548303921/samples/2019/muaxuancuaem/muaxuancuaem1.jpg) 


<p align="justify">Công ty Dịch vụ Agribank đến với Chương trình là sự sẻ chia, tiếp sức cho các em học sinh vùng cao tới trường, thể hiện tinh thần thường xuyên trong hoạt động từ thiện và an sinh xã hội của doanh nghiệp. Chương trình được tổ chức vào thời điểm đón Tết Kỷ Hợi 2019 mang ý nghĩa nhân văn to lớn với học sinh vùng cao phía Bắc có một cái tết vui và ấm áp hơn.</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1548303920/samples/2019/muaxuancuaem/muaxuancuaem2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1548303920/samples/2019/muaxuancuaem/muaxuancuaem3.jpg)


<p aligne ="justify">Công ty Dịch vụ Agribank đã và sẽ tiếp tục triển khai các hoạt động “Tương thân-Tương ái”.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1548303927/samples/2019/muaxuancuaem/muaxuancuaem4.jpg)
