---
title: Chương trình "Áo ấm đến trường" của Công ty TNHH Một thành viên Dịch vụ Ngân hàng Nông nghiệp Việt Nam
categories: hoạt động nội bộ
date: 2017-01-16 15:10:35
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304170/samples/2017/tuthiencaobang/caobang_preview.jpg

---

Chương trình "Áo ấm đến trường” của Công ty TNHH Một thành viên Dịch vụ Ngân hàng Nông nghiệp Việt Nam phối hợp Agribank Chi nhánh Cao Bằng trao tặng; áo ấm, cặp sách, đồ dùng học tập cho các em học sinh trường Mầm non, Tiểu học, THCS xã Nội Thôn, huyện Hà Quảng, tỉnh Cao Bằng .

Vào những ngày giá rét nhất, nhà nhà chuẩn bị đón Xuân 2017.Công ty TNHH Một thành viên Dịch vụ Ngân hàng Nông nghiệp Việt Nam phối hợp Agribank Chi nhánh Cao Bằng trao tặng 406 áo ấm cùng cặp sách, đồ dùng học tập cho các em học sinh trường Mầm non, Tiểu học, THCS xã Nội Thôn, huyện Hà Quảng, tỉnh Cao Bằng .

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304169/samples/2017/tuthiencaobang/caobang1.jpg)
*Ông Đỗ Đình Hồng - Chủ tịch Công ty Dịch vụ Agribank  trao quà cho các em học sinh.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304169/samples/2017/tuthiencaobang/caobang2.jpg)

*Đoàn Công ty Dịch vụ Agribank  trao quà cho các em học sinh.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304169/samples/2017/tuthiencaobang/caobang3.jpg)

*Các em học sinh xã Nội Thôn, huyện Hà Quảng, tỉnh Cao Bằng đón nhận quà trong Chương trình "Áo ấm đến trường”*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304169/samples/2017/tuthiencaobang/caobang4.jpg)

*Trường tiểu học- trung học Nội Thôn nằm trên cao nguyên đá cheo leo huyện Hà Quảng, tỉnh Cao Bằng.*

Thông qua các hoạt động an sinh xã hội, Công ty Dịch vụ Agribank và cùng hệ thống Agribank phát huy trách nhiệm xã hội của doanh nghiệp đối với cộng đồng, tích cực cùng Chính phủ, góp phần đảm bảo an sinh xã hội, phát huy truyền thống tương thân tương ái tốt đẹp của dân tộc Việt Nam.
