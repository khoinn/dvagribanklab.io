---
title: Lễ Kết nạp Đảng viên 2021
date: 2021-01-05 15:00:40
categories: "hoạt động nội bộ"
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1609902043/samples/2021/ketnapdang/kenap_preview.jpg
---

Chiều ngày 05 tháng 01 năm 2021 tại Hội trường Công ty Dịch vụ Agribank, được sự đồng ý của Đảng ủy, Chi bộ 1 tổ chức Lễ kết nạp Đảng viên mới cho Đồng chí Nguyễn Thị Bình và trao Quyết định công nhận Đảng viên chính thức cho đồng chí Vũ Công Định.

<p align="justify">Tới dự Lễ có Đồng chí Nguyễn Hải Long - Bí thư Đảng ủy- Chủ tịch Công ty, về phía các Chi bộ có Bí thư Chi bộ 2, Chi bộ 3, ngoài ra còn có các đồng chí đại diện BCH Công đoàn BP 1, BCH Đoàn thanh niên, các quần chúng trong diện bồi dưỡng kết nạp Đảng cùng toàn thể các đồng chí Đảng viên trong Chi bộ 1.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1609902044/samples/2021/ketnapdang/kenap1.jpg)

*Bí thư Chi bộ 1 trao Quyết định kết nạp Đảng viên cho đồng chí Nguyễn Thị Bình*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1609902043/samples/2021/ketnapdang/kenap2.jpg)

*Bí thư Chi bộ 1 trao Quyết định công nhận Đảng viên chính thức cho đồng chí Vũ Công Định*

<p align="justify">Tại buổi lễ Đồng chí Nguyễn Hải Long - Bí thư Đảng ủy có đôi lời phát biểu căn dặn và chỉ đạo sâu sắc tới từng Đảng viên.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1609902044/samples/2021/ketnapdang/kenap3.jpg)

*Bí thư Đảng ủy phát biểu tại lễ kết nạp Đảng viên*

<p align="justify">Buổi lễ trao Quyết định kết nạp Đảng và công nhận Đảng viên chính thức đã diễn ra trong không khí trang trọng, nghiêm túc và hoàn tất theo quy định của Điều lệ Đảng.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1609902044/samples/2021/ketnapdang/kenap4.jpg)
















  







