---
title: Lễ công bố quyết định giao nhiệm vụ Phụ trách kế toán Công ty Dịch vụ Agribank.
date: 2020-11-09 10:18:40
categories: "hoạt động nội bộ"
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604991381/samples/2020/giaonhiemvukt/giaonhiemvukt_preview.jpg 
---

  Ngày 09/11/2020, tại hội trường Công ty Dịch vụ Agribank diễn ra Lễ công bố Quyết định của Tổng Giám đốc Agribank về việc Phụ trách kế toán Công ty Dịch vụ Agribank.

Bà Đoàn Thu Hà PTP. Phụ trách Hành chính Nhân sự Công ty đọc Quyết định số 2332/QĐ-NHNo-TCTL ngày 05/11/2020 của Tổng Giám đốc Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt nam về việc giao nhiệm vụ Phụ trách kế toán Công ty Dịch vụ Agribank đối với ông Lê Ngọc Anh.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604991382/samples/2020/giaonhiemvukt/giaonhiemvukt1.jpg)

*Ông Phạm Văn Thành P.TGD Phụ trách trao Quyết định và chúc mừng ông Lê Ngọc Anh*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604991382/samples/2020/giaonhiemvukt/giaonhiemvukt2.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604991382/samples/2020/giaonhiemvukt/giaonhiemvukt4.jpg)

*Ông Lê Ngọc Anh phát biểu nhận nhiệm vụ*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604991382/samples/2020/giaonhiemvukt/giaonhiemvukt3.jpg)












  







