---
title: Lễ công bố quyết định bổ nhiệm Phó Tổng Giám đốc Công ty Dịch vụ Agribank
categories: hoạt động nội bộ
date: 2019-6-14 
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1560481112/samples/2019/bnpt2/bnpt_preview.jpg
---

Ngày 14/6/2019, tại Công ty Dịch vụ Agribank diễn ra Lễ công bố và trao quyết định của Chủ tịch Hội đồng Thành viên Agribank về việc bổ nhiệm Phó Tổng Giám đốc Công ty Dịch vụ Agribank đối với ông Phan Việt Trung. Đại diện Agribank tại lễ công bố quyết định có: Ông Phạm Hoàng Đức: Đảng ủy viên - Thành viên HĐTV Agribank, Bà Phạm Hồng Thu: Trưởng ban Đầu tư Agribank, Ông Nguyễn Văn Hà: Thành viên chuyên trách UBNS Agribank đọc Quyết định của Chủ tịch Hội đồng Thành viên Agribank bổ nhiệm ông Phan Việt Trung chức vụ Phó Tổng Giám đốc Công ty Dịch vụ Agribank.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1560481112/samples/2019/bnpt2/bnpt2.jpg)
*Ông Nguyễn Văn Hà – Thành viên chuyên trách UBNS Agribank đọc quyết định bổ nhiệm*

Ông Phạm Hoàng Đức đại diện Agibank đã trao quyết định và giao nhiệm vụ, đồng thời ghi nhận sự đóng góp và hoàn thành nhiệm của Công ty cũng như cá nhân đồng chí Phan Việt Trung.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1560481112/samples/2019/bnpt2/bnpt3.jpg)
*Ông Phạm Hoàng Đức - Thành viên HĐTV trao quyết định*

Bí thư Đảng ủy, Chủ tịch - Tổng Giám đốc Công ty Dịch vụ Agribank ông Đỗ Đình Hồng thay mặt Công ty nhận nhiệm vụ chung và giao trách nhiệm cá nhân cho đồng chí Phan Việt Trung trên cương vị mới. Đồng thời ông Đỗ Đình Hồng gửi lời cảm ơn đến Đảng ủy, Ban Lãnh đạo Agribank.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1560481112/samples/2019/bnpt2/bnpt1.jpg)
*Ông Đỗ Đình Hồng – Chủ tịch, Tổng giám đốc phát biểu tại lễ trao quyết định*

Tại buổi lễ, ông Phan Việt Trung phát biểu nhận nhiệm vụ, nói lời cảm ơn và hứa trước ban lãnh đạo, tập thể cố gắng hoàn thành tốt nhiệm vụ giữ vững phát huy vai trò trên cương vị mới, xứng đáng với sự tin tưởng của Ban lãnh đạo, tập thể góp phần tích cực vào sự phát triển bền vững của Công ty.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1560481112/samples/2019/bnpt2/bnpt5.jpg) 
*Ông Phan Việt Trung - nhận nhiệm vụ và phát biểu tại buổi lễ*
