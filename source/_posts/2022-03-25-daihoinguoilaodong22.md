---
title: Công ty Dịch vụ Agribank Tổ chức Hội nghị đại biểu người lao động năm 2022
Date: 2022-3-25
categories: hoạt động nội bộ
tacgia: Kinh doanh Tiếp thị ASC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636437/samples/2022/HNNLD22/NLD22_preview.jpg

---
<p align="justify"> Sáng ngày 27/3/2022, tại Hội trường Khách sạn Novotel, số 50 Đường Trần Phú, Thành phố Nha Trang, tỉnh Khánh Hòa, Công ty Dịch vụ Agribank tổ chức Hội nghị đại biểu người lao động năm 2022. Hội nghị có sự tham dự của 79 đại biểu là cán bộ, nhân viên từ khối Văn phòng Công ty và 04 đơn vị trực thuộc khu vực phía Bắc và phía Nam đại diện cho gần 400 người lao động toàn Công ty.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636437/samples/2022/HNNLD22/NLD22_preview.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_3.jpg)


*Đồng chí Vũ Mạnh Toàn-Tổng Giám đốc Công ty báo cáo tổng kết hoạt động sản xuất kinh doanh năm 2021, kế hoạch và giải pháp triển khai năm 2022*
<p align="justify">Hội nghị đã nghe và thống nhất nội dung Báo cáo tổng kết hoạt động sản xuất kinh doanh năm 2021, kế hoạch và giải pháp triển khai năm 2022; Báo cáo tổng kết hoạt động Công đoàn năm 2021, phương hướng nhiệm vụ năm 2022; Báo cáo kết quả thực hiện Thỏa ước lao động tập thể; Báo cáo kết quả thực hiện chính sách đối với người lao động; Báo cáo tình hình sử dụng quỹ khen thưởng, phúc lợi; Báo cáo kết quả hoạt động của Ban thanh tra nhân dân; Báo cáo thẩm tra tư cách đại biểu và báo cáo tham luận của các đơn vị.</p> 
<p align="justify">Năm 2021, trước tình hình dịch bệnh Covid-19 diễn biến phức tạp, dưới sự quan tâm, ủng hộ, chỉ đạo sát sao của Agribank cùng sự nỗ lực, đồng lòng, quyết liệt hành động từ Ban lãnh đạo và tập thể cán bộ, nhân viên, Công ty Dịch vụ Agribank đã hoàn thành vượt mức, toàn diện các chỉ tiêu kế hoạch năm 2021 Agribank giao; đảm bảo thu nhập của người lao động cao hơn mức thu nhập bình quân năm 2021; điều kiện làm việc, cơ sở vật chất từng bước được cải thiện, nâng cao; thực hiện đầy đủ các nội dung, cam kết theo Thoả ước lao động tập thể hiện hành.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_4.jpg)
*Đồng chí Trần Hồng Thắng-Chủ tịch CĐCS, Phó Tổng Giám đốc Công ty báo cáo tổng hợp các ý kiến tại Hội nghị*

<p align="justify">Tại Hội nghị, đồng chí Trần Hồng Thắng - Chủ tịch CĐCS, Phó Tổng Giám đốc Công ty cũng đã báo cáo tổng hợp các ý kiến đóng góp Thỏa ước Lao động tập thể hiện hành. Hội nghị cũng đã bầu ra 14 đồng chí đại diện cho tập thể người lao động tham gia đối thoại.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_6.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chúc mừng các đồng chí đại diện cho người lao động tham gia đối thoại*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_5.jpg)
  *Đồng chí Nguyễn Hải Long, Chủ tịch Công ty phát biểu chỉ đạo tại Hội nghị*

<p align="justify">Phát biểu chỉ đạo tại Hội nghị, đồng chí Nguyễn Hải Long - Ủy viên BCH đảng bộ, Phó Tổng Giám đốc Agribank, Bí thư Đảng ủy, Chủ tịch Công ty đánh giá cao sự đoàn kết, đồng lòng của Ban lãnh đạo và người lao động trong Công ty đã tạo nên sức mạnh tập thể, giúp Công ty đạt được kết quả tốt trong năm 2021. Đồng chí yêu cầu lãnh đạo Công ty cùng với Công đoàn tiếp tục phối hợp để tạo điều kiện tốt nhất cho người lao động trong công việc và trong cuộc sống, tạo động lực cho người lao động cống hiến. Đồng chí cũng nêu định hướng, giải pháp thực hiện nhiệm vụ của Công ty năm 2022 và các năm tiếp theo, đồng thời giải đáp kiến nghị của các đơn vị.</p>
<p align="justify">Tiếp thu ý kiến chỉ đạo của đồng chí Nguyễn Hải Long, đồng chí Vũ Mạnh Toàn-Tổng Giám đốc Công ty kêu gọi toàn thể người lao động, các phòng nghiệp vụ và các đơn vị trực thuộc cần chủ động phối hợp, đoàn kết để đạt được mục tiêu chung là hoàn thành kế hoạch sản xuất kinh doanh năm 2022 và đảm bảo việc làm, nâng cao đời sống cho người lao động.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_9.jpg)
 *Các đại biểu thống nhất thông qua Nghị quyết Hội Nghị*

<p align="justify">Tại Hội nghị, 79 đại biểu đại diện cho ý chí, nguyện vọng, trí tuệ của gần 400 cán bộ, nhân viên toàn Công ty đã thảo luận tích cực và thống nhất thông qua Nghị quyết Hội Nghị.</p>
<p align="justify">Hội nghị đại biểu Người lao động Công ty Dịch vụ Agribank năm 2022 đã thành công tốt đẹp. Hội nghị là nơi người lao động được phát huy quyền dân chủ trực tiếp; tạo điều kiện để người lao động được biết, tham gia ý kiến liên quan đến quyền lợi, nghĩa vụ và trách nhiệm của mình. Thông qua việc thực hiện dân chủ, xây dựng quan hệ lao động hài hòa, ổn định, phòng ngừa và hạn chế những tranh chấp trong doanh nghiệp; đồng thời, góp phần thực hiện dân chủ trong khuôn khổ pháp luật, xây dựng doanh nghiệp phát triển bền vững./.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648639592/samples/2022/HNNLD22/IMG_0141.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_7.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty tặng hoa cho đội văn nghệ*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_8.jpg)
 *Đồng chí Nguyễn Hải Long, Chủ tịch Công ty tặng hoa cho đội văn nghệ*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_10.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chụp ảnh lưu niệm cùng người lao động tại Hội nghị*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_11.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chụp ảnh lưu niệm cùng người lao động tại Hội nghị*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_12.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chụp ảnh lưu niệm cùng người lao động tại Hội nghị*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_13.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chụp ảnh lưu niệm cùng người lao động tại Hội nghị*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_14.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chụp ảnh lưu niệm cùng người lao động tại Hội nghị*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1648636356/samples/2022/HNNLD22/NLD22_15.jpg)
*Đồng chí Nguyễn Hải Long, Chủ tịch Công ty chụp ảnh lưu niệm cùng người lao động tại Hội nghị*

