---
title: GIAO LƯU THỂ THAO CHÀO MỪNG KỶ NIỆM 32 NĂM NGÀY THÀNH LẬP VỤ KIỂM TOÁN NỘI BỘ (27/12/1990 - 27/12/2022), NGÀY QUỐC TẾ ĐOÀN KẾT TOÀN NHÂN LOẠI (20/12) VÀ TIẾN TỚI CHÀO MỪNG ĐẠI HỘI CÔNG ĐOÀN CÁC CẤP
Date: 2022-12-17
categories: hoạt động nội bộ
tacgia: Kinh doanh Tiếp thị ASC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_1.jpg

---



<p align="justify">Ngày 17/12/2022, Công đoàn Công ty Dịch vụ Agribank tham gia phối hợp tổ chức Chương trình giao hữu thể thao cùng Công đoàn cơ sở Vụ Kiểm toán Nội bộ, Công đoàn Học viện Ngân hàng, Công đoàn cơ sở VAMC và Công đoàn Agribank Chi nhánh Thăng Long. Chương trình hướng tới chào mừng kỷ niệm 32 năm ngày thành lập Vụ Kiểm toán nội bộ (27/12/1990 - 27/12/2022), chào mừng Ngày Quốc tế đoàn kết nhân loại (20/12 hàng năm) và tiến tới chào mừng Đại hội Công đoàn các cấp.<p>
Đến dự Chương trình giao lưu thể thao có sự hiện diện của các đồng chí:
    . Đồng chí Lê Thị Quyên - Ủy viên Ban Thường vụ Công đoàn Ngân hàng Việt Nam - Phó Chủ tịch Thường trực Công đoàn Cơ quan NHNN Trung ương;
    . Đồng chí Lê Quốc Nghị - Vụ trưởng Vụ Kiểm toán nội bộ - Chủ tịch Công đoàn cơ quan NHNN Trung ương;
    . Đồng chí Mai Thanh Quế - Phó Giám đốc - Chủ tịch Công đoàn Học viện Ngân hàng;
    . Đồng chí Đặng Đình Thích - Phó Tổng Giám đốc Công ty Quản lý tài sản VAMC;
    . Đồng chí Lê Tuấn Minh - Phó Giám đốc - Chủ tịch Công đoàn.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_1.jpg)

Về phía Công ty Dịch vụ Agribank có sự tham dự của đồng chí Trần Duy Hưng - Chủ tịch Công ty; đồng chí Trần Hồng Thắng - Phó Tổng Giám đốc PTĐH - Chủ tịch Công đoàn Công ty; đồng chí Trịnh Ngọc Tùng - Phó Tổng Giám đốc Công ty; đồng chí Nguyễn Việt Cường - Phó Chủ tịch Công đoàn Công ty. Cùng với đó là sự tham gia góp mặt của các vận động viên, cán bộ nhân viên, cổ động viên đến từ 05 đơn vị.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_5.jpg)

Tham gia các nội dung thi đấu giao hữu cùng các đơn vị bạn, đội vận động viên Công ty Dịch vụ Agribank đã mang về thành tích đáng kể: 
🏅 Giải Nhất môn bóng bàn đôi nam trên 45
🏅 Giải Nhất môn tennis đôi nam dưới 45
🏅 Giải Nhì môn bóng đá nam
🏅 Đồng giải Ba môn kéo co

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_6.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_7.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1671501768/samples/2022/17_12_22/gioluutt17_12_2022_8.jpg)

Hoạt động ý nghĩa này đã phát huy tinh thần đoàn kết, giao lưu giữa các tổ chức Công đoàn trong ngành Ngân hàng , qua đó tạo sự kết nối giữa các công chức, viên chức, người lao động trong toàn ngành. Đồng thời, nêu cao tinh thần rèn luyện thể chất, thể dục thể thao của các công chức, viên chức và người lao động.









