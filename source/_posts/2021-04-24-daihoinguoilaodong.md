---
title: Hội nghị đại biểu Người lao động Công ty Dịch vụ Agribank năm 2021.
Date: 2021-4-24 15:10:35
categories: hoạt động nội bộ
tacgia: Kinh doanh Tiếp thị ASC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408270/samples/2021/dhnld2021/nld2021_preview.jpg 

---
Sáng ngày 24/4/2021, Công ty Dịch vụ Agribank đã tổ chức Hội nghị đại biểu Người lao động năm 2021.

<p align="justify"> Hội nghị đã được nghe Đồng chí Vũ Mạnh Toàn, Tổng Giám đốc Công ty trình bày Báo cáo kết quả hoạt động sản xuất kinh doanh năm 2020 và thực hiện Kế hoạch SXKD năm 2021: Năm 2020 do ảnh hưởng của đại dịch Covid-19, sản xuất kinh doanh của công ty gặp nhiều khó khăn, đối diện rủi ro về gia tăng chi phí hoạt động, giảm sút khối lượng cung cấp sản phẩm, dịch vụ và hệ lụy tài chính, công nợ phát sinh đột xuất, tồn đọng từ qúa khứ. Được sự quan tâm, chỉ đạo sâu sát, kịp thời của Agribank cùng sự nỗ lực đồng lòng, quyết liệt hành động từ Ban Lãnh đạo và tập thể CBCNV, Công ty đã hoàn thành vượt mức chỉ tiêu kế hoạch Agribank giao, đảm bảo đủ công ăn việc làm cho người lao động với mức thu nhập trung bình khá trong hệ thống.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_1.jpg)

<p align="justify">Để thực hiện hoàn thành mục tiêu kế hoạch năm 2021: Nâng cao năng lực quản trị điều hành; tăng cường công tác chỉ đạo điều hành sản xuất thông suốt, nhanh chóng, an toàn, hiệu quả; bổ sung sửa đổi, thay thế, hoàn thiện hệ thống văn bản quy định phục vụ công tác quản lý, điều hành hoạt động của Công ty; thực hiện giao khoán tài chính năm 2021; quyết tâm hoàn thành chỉ tiêu kế hoạch kinh doanh năm 2021 với tổng doanh thu 320 tỷ đồng, lợi nhuận trước thuế đạt 22,45 tỷ đồng, đảm bảo thu nhập và việc làm cho người lao động.</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_4.jpg)

<p align="justify">Thay mặt đoàn Chủ tịch, Đồng chí Phan Việt Trung, Chủ tịch Công đoàn cơ sở đã trình bày báo cáo kết quả hoạt động công tác công đoàn năm 2020; phương hướng, nhiệm vụ năm 2021. Theo đó, phương hướng, nhiệm vụ trọng tâm hoạt động công đoàn năm 2021: Nỗ lực phấn đấu hoàn thành vượt mức các chỉ tiêu kế hoạch kinh doanh Agribank giao; duy trì việc làm ổn định; quyền lợi của người lao động đi đôi với trách nhiệm, tinh thần làm việc để hoàn thành công việc đạt chất lượng, hiệu quả, bảo đảm thu nhập cho người lao động; tổng hợp các kiến nghị từ người lao động, tham gia xây dựng, sửa đổi các nội quy, quy chế liên quan đến quyền lợi và nghĩa vụ của người lao động.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_5.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_6.jpg)


<p align="justify">Tại Hội nghị, đại diện Phòng Tổ chức Hành chính đã trình bày báo cáo kết quả thực hiện chính sách đối với người lao động giải đáp những kiến nghị của người lao động về quy chế trả lương trả thưởng và nhiều nội dung liên quan khác. Đại diện BCH Công đoàn đã trình bày báo cáo công khai về việc trích lập và sử dụng quỹ phúc lợi, trích kinh phí Công đoàn, bảo hiểm xã hội, bảo hiểm y tế, bảo hiểm thất nghiệp...</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408267/samples/2021/dhnld2021/nld2021_7_1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_7.jpg)

<p align="justify">Hội nghị đã dành phần lớn thời gian để người lao động trình bày tham luận về các vấn đề liên quan đến quyền lợi, chế độ của người lao động; các giải pháp để hoàn thành tốt nhiệm vụ trọng tâm trong năm 2021.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_8.jpg)

<p align="justify"> Phát biểu tại Hội nghị, đại diện người sử dụng lao động, Chủ tịch Công ty - Ông Nguyễn Hải Long đã đánh giá cao và biểu dương thành tích đóng góp rất quan trọng của tập thể cán bộ, nhân viên công ty trong năm 2020. Với vai trò đại diện người sử dụng lao động đã kịp thời điều chỉnh chế độ chính sách cho người lao động phù hợp với tình hình tài chính của Công ty. Bên cạnh đó, Chủ tịch đã thẳng thắn chỉ ra những tồn tại, hạn chế cần được chấn chỉnh, khắc phục tại công ty trong thời gian tới. Sang năm 2021, dự báo tình hình sản xuất kinh doanh của công ty sẽ còn tiếp tục gặp nhiều khó khăn, Chủ tịch đã đề nghị tập thể người lao động công ty tăng cường và nâng cao chất lượng công việc, thực hiện trách nhiệm, nghĩa vụ của người lao động, đồng thời nâng cao công tác điều hành, xử lý, phối hợp của Ban Tổng Giám đốc thúc đẩy hoạt động sản xuất kinh doanh của công ty, đặc biệt tập trung vào nhiệm vụ trọng tâm như: thu hồi công nợ, tài sản của công ty; đề ra các giải pháp trong quản lý, sử dụng vốn tài sản của công ty; xây dựng chế độ đãi ngộ cho người lao động đúng các quy định pháp luật hiện hành vừa phù hợp với tình hình tài chính hiện có của công ty…</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_9.jpg)

*Đại diện người sử dụng lao động: Ông Nguyễn Hải Long, Chủ tịch Công ty và đại diện người Lao động: Chủ tịch công đoàn cơ sở Ông Phan Việt Trung ký kết “Thỏa ước lao động tập thể”*

<p align="justify">Tiếp theo chương trình, Hội nghị đã diễn ra Lễ ký kết "Thỏa ước lao động tập thể" giữa Đại diện người sử dụng lao động: Chủ tịch Công ty - Ông Nguyễn Hải Long với Đại diện người lao động: Chủ tịch Công đoàn cơ sở - Ông Phan Việt Trung.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_10.jpg)

<p align="justify">Hội nghị ra mắt Ban đại diện tập thể người lao động tham gia đối thoại năm 2021. Tại Hội nghị, 94 đại biểu đại diện cho ý chí, nguyện vọng, trí tuệ của gần 400 cán bộ nhân viên toàn Công ty đã thảo luận tích cực và thống nhất thông qua Nghị quyết Hội Nghị.</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_12.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_13.jpg)

<p align="justify">Hội nghị đại biểu Người lao động Công ty Dịch vụ Agribank năm 2021 đã thành công tốt đẹp. Hội nghị là nơi người lao động được phát huy quyền dân chủ trực tiếp; tạo điều kiện để người lao động được biết, tham gia ý kiến liên quan đến quyền, lợi ích, nghĩa vụ và trách nhiệm của người lao động. Thông qua việc thực hiện dân chủ, xây dựng quan hệ lao động hài hòa, ổn định, phòng ngừa và hạn chế những tranh chấp trong doanh nghiệp; đồng thời góp phần thực hiện dân chủ trong khuôn khổ pháp luật, xây dựng doanh nghiệp phát triển bền vững./.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_14.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_15.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_16.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1619408265/samples/2021/dhnld2021/nld2021_17.jpg)

