---
title: Công ty Dịch vụ Agribank chung tay ủng hộ phòng chống dịch Covid-19
Date: 2021-08-02 15:10:35
categories: "hoạt động nội bộ"
tacgia: Phòng Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1627956411/samples/2021/ungho/ungho_preview.jpg  

---

<p align="justify">Trước tình hình dịch Covid-19 đang diễn biến phức tạp và bùng phát mạnh tại nhiều tỉnh, thành phố trên cả nước, đặc biệt là tại Thành phố Hồ Chí Minh, gây ra tác động nhiều mặt đến đời sống nhân dân và kinh tế, xã hội của đất nước, nhằm chung tay trong công tác phòng chống dịch bệnh với quyết tâm, mong muốn ngăn chặn, đẩy lùi dịch Covid-19, ngày 27/7/2021 vừa qua, cán bộ, nhân viên Công ty Dịch vụ Agribank đã tham gia đóng góp, ủng hộ cho hoạt động phòng chống dịch Covid-19 của Viện Đào tạo Y học dự phòng và Y tế công cộng thuộc Trường Đại học Y Hà Nội.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1627956415/samples/2021/ungho/ungho1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1627956415/samples/2021/ungho/ungho2.jpg) 


<p align="justify">Trong thời gian qua, hưởng ứng lời kêu gọi của Đảng, Nhà nước, với tinh thần tương thân tương ái cùng truyền thống nhân văn sâu sắc, nhiều cán bộ, sinh viên của Viện Đào tạo Y học dự phòng và Y tế công cộng đã tình nguyện xung phong lên đường chi viện và hỗ trợ cho các địa phương chống dịch, tham gia xét nghiệm và triển khai công tác tiêm phòng vắc xin phòng Covid-19.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1627956415/samples/2021/ungho/ungho3.jpg) 

<p align="justify">Bằng hành động thiết thực ủng hộ cho hoạt động phòng chống dịch của Viện Đào tạo Y học dự phòng và Y tế công cộng, cán bộ, nhân viên Công ty Dịch vụ Agribank mong muốn tiếp thêm sức mạnh và nguồn động viên đối với tập thể cán bộ, giảng viên, bác sỹ, điều dưỡng, sinh viên, học viên của Viện, góp sức cùng Chính phủ trong cuộc chiến chống đại dịch Covid-19 đầy cam go, thử thách.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1627956415/samples/2021/ungho/ungho4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1627956415/samples/2021/ungho/ungho5.jpg)  






