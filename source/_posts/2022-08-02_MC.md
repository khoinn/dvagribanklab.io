---
title: CÔNG TY DỊCH VỤ AGRIBANK TỔ CHỨC LỄ MỪNG CÔNG ĐOÀN THỂ THAO CÔNG TY THAM DỰ HỘI THAO AGRIBANK LẦN THỨ IX - KHU VỰC HÀ NỘI NĂM 2022.
Date: 2022-8-02
categories: hoạt động nội bộ
tacgia: Kinh doanh Tiếp thị ASC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430009/samples/2022/mungcongTT/preview_mc.jpg

---

<p align="justify">Chiều ngày 01/8/2022, Công ty Dịch vụ Agribank tổ chức Lễ mừng công đoàn thể thao Công ty tham dự Hội thao Agribank lần thứ IX - Khu vực Hà Nội năm 2022.
Tham dự Lễ mừng công có sự hiện diện của đồng chí Nguyễn Hải Long - Phó Tổng Giám đốc Agribank, Bí thư Đảng ủy, Chủ tịch Công ty; đồng chí Trần Hồng Thắng - Phó Tổng Giám đốc Phụ trách điều hành, Chủ tịch Công đoàn cơ sở Công ty; các đồng chí là Trưởng các phòng tại Trụ sở chính, Giám đốc các đơn vị trực thuộc, đại diện BCH Đoàn thanh niên cùng các huấn luyện viên, vận động viên, ban hậu cần và các cổ động viên.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc1.jpg)
*Đồng chí Nguyễn Việt Cường - Trưởng đoàn thể thao báo cáo tại Lễ mừng công*

<p align="justify">Tại Lễ mừng công, đồng chí Nguyễn Việt Cường - Trưởng đoàn thể thao đã đọc báo cáo kết quả và những thành tích đã đạt được trong Hội thao Agribank lần thứ IX năm 2022 - Khu vực Hà Nội. Đoàn thể thao của Công ty đã giành được thành tích cao, đạt giải Nhì toàn đoàn với rất nhiều giải nhất, nhì ở các bộ môn, cụ thể là: Giải nhất môn Bóng bàn lứa tuổi trên 45 (đôi nam); Giải nhất môn Cầu lông lứa tuổi dưới 45 (đôi nam nữ); Giải nhất môn Tennis lứa tuổi dưới 45 (đôi nam); Giải nhất môn Bóng chuyền da nam; Giải nhì môn Bóng đá nam.<p> 
Thay mặt đoàn thể thao, đồng chí Nguyễn Việt Cường đã bày tỏ lòng cám ơn sâu sắc đối với sự quan tâm của Chủ tịch, Tổng Giám đốc, Ban chấp hành Công đoàn Công ty đã tạo mọi điều kiện thuận lợi cho đoàn luyện tập, thi đấu.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc2.jpg)
*Đồng chí Trần Hồng Thắng - Phó Tổng Giám đốc Phụ trách điều hành, Chủ tịch Công đoàn Công ty phát biểu ý kiến và giao nhiệm vụ*

<p align="justify">Đồng chí Trần Hồng Thắng- Phó Tổng Giám đốc Phụ trách điều hành, Chủ tịch Công đoàn cơ sở Công ty đã biểu dương và ghi nhận sự cố gắng, nỗ lực, tinh thần thi đấu của các vận động viên, đồng thời giao nhiệm vụ cho đoàn thể thao tập trung luyện tập để chuẩn bị tốt nhất cho Hội thao Agribank toàn quốc sẽ diễn ra trong thời gian tới.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc3.jpg)
*Đồng chí Nguyễn Hải Long - Phó Tổng Giám đốc Agribank, Chủ tịch Công ty phát biểu và chỉ đạo tại Lễ mừng gitcông*

<p align="justify">Phát biểu chỉ đạo tại Lễ mừng công, đồng chí Nguyễn Hải Long - Phó Tổng Giám đốc Agribank, Bí thư Đảng ủy, Chủ tịch Công ty chúc mừng đoàn thể thao đã nỗ lực, khắc phục khó khăn, thi đấu nhiệt huyết với tinh thần cao thượng, trung thực và đã đạt được kết quả cao trong Hội thao. Đồng chí khẳng định Ban lãnh đạo Công ty sẽ tạo mọi điều kiện thuận lợi cho các vận động viên luyện tập để tham dự Hội thao Agribank toàn quốc, đồng thời cũng nhắc nhở, động viên các vận động viên vừa tích cực tập luyện nhưng cũng không quên nhiệm vụ sản xuất kinh doanh.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659431298/samples/2022/mungcongTT/mc13.jpg)
*Ban lãnh đạo Công ty chúc mừng, trao hoa và phần thưởng cho các vận động viên, đoàn vận động viên tham dự Hội thao.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc5.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc6.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc7.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc8.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1659430007/samples/2022/mungcongTT/mc9.jpg)


Để vinh danh thành tích đã đạt được cũng như khích lệ tinh thần thi đấu của đoàn thể thao Công ty, Ban lãnh đạo Công ty đã tặng hoa chúc mừng và trao phần thưởng cho các vận động viên, đoàn vận động viên tham dự Hội thao.

Hội thao Agribank là sự kiện thể thao của Agribank quy tụ nhiều vận động viên hàng đầu; mang đến sân chơi lành mạnh, bổ ích và tạo cơ hội giao lưu học hỏi lẫn nhau, nâng cao tinh thần đoàn kết giữa các chi nhánh và các đơn vị trong hệ thống Agribank./.









