---
title: Lễ tổng kết Giải bóng đá Agribank Cup 2015 Đội bóng Cty Dịch vụ Agribank giành chức vô địch
date: 2015-12-17
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304530/samples/thang12/traogiaidoibong.jpg

---

Tối 6-11-2015 tại Hội trường Công ty Dịch vụ Agribank  đã  diễn ra
“Lễ tổng kết Giải bóng đá Agribank Cup  2015”.

Đến dự lễ về phía Ngân hàng Nông nghiệp và Phát triển Nông thôn đại diện có đồng chí Đồng Đức Giang, Đảng ủy viên, Bí Thư Đoàn Thanh niên Agribank, về phía Công ty có đồng chí Đỗ Đình Hồng, Bí thư Đảng ủy, Chủ tịch Công ty Dịch vụ Agribank và cán bộ công nhân viên cùng đội bóng.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304530/samples/thang12/traogiaidoibong_2.jpg)


*Niềm hân hoan cùng chung vui chiến thắng*

Tại lễ tổng kết các đồng chí lãnh đạo biểu dương tinh thần luyện tập thi đấu và thành tích của đội bóng Công ty dịch vụ Agribank vượt qua 31 đội bóng cùng tranh tài, trở thành nhà vô địch Giải bóng đá Agribank Cup  2015. Đây là lần thứ 2 liên tiếp đội bóng Công ty Dịch vụ Agribank giành  Cup Agribank 

Đồng chí Đỗ Đình Hồng, Bí thư Đảng ủy, Chủ tịch Công ty Dịch vụ Agribank trao phần thưởng động viên tinh thần, khích lệ những thành tích đã đạt được cho đội bóng. Khuyến khích anh em trong đội bóng tham gia luyện tập để giành chiến thắng mùa giải năm sau.

Giải bóng đá Agribank Cup 2015 là giải bóng đá do Đoàn Thanh niên Ngân hàng Nông nghiệp và Phát triển Nông thôn  tổ chức hàng năm, tham dự giải là cán bộ, nhân viên trong hệ thống Agribank. Công ty Dịch vụ Agribank từ nhiều năm qua đã tích cực tham gia và có hiệu quả trong các giải thi đấu thể thao do Đoàn Thanh niên Agribank tổ chức với tinh thần rèn luyện sức khỏe, đoàn kết, giao lưu học hỏi lẫn nhau, nâng cao thể lực, trí tuệ để hoàn thành tốt công việc.

Một số hình ảnh thi đấu và nhận giải

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db5.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db6.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304527/samples/thang12/db7.jpg)