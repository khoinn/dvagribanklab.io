---
title: CÔNG TY DỊCH VỤ AGRIBANK CHÚC MỪNG LÃNH ĐẠO AGRIBANK CHI NHÁNH TỈNH BẮC GIANG VÀ AGRIBANK CHI NHÁNH TỈNH BẮC NINH
categories: hoạt động nội bộ
date: 2022-11-04
tacgia: Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668732569/samples/2022/chucmungBGBN/BNBG1.jpg

---

<p align="justify">Ngày 01/11/2022, Ban lãnh đạo Công ty Dịch vụ Agribank có chuyến thăm, gặp gỡ và chúc mừng lãnh đạo mới tại 02 Chi nhánh bạn. Đến chúc mừng có đồng chí Trần Duy Hưng - Chủ tịch Công ty, đồng chí Trần Thu Thủy - Phó Tổng Giám Đốc Công ty, đồng chí Phạm Ngọc Ruyển - Giám đốc Nhà in Ngân hàng I cùng các đồng chí Trưởng phòng nghiệp vụ.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668732569/samples/2022/chucmungBGBN/BNBG1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668732569/samples/2022/chucmungBGBN/BNBG2.jpg)

<p align="justify">Đồng chí Trần Duy Hưng đã bày tỏ sự vui mừng, tặng hoa chúc mừng đồng chí Nguyễn Quốc Hưng được giao nhiệm vụ giữ chức vụ Giám đốc Agribank Chi nhánh tỉnh Bắc Ninh và đồng chí Lương Văn Nội được giao nhiệm vụ Phụ trách điều hành Agribank Chi nhánh tỉnh Bắc Giang. Chúc các đồng chí luôn hoàn thành xuất sắc nhiệm vụ, gặt hái được nhiều thành công trên cương vị mới, trọng trách mới. Đồng thời, chỉ đạo và lãnh đạo Chi nhánh hoàn thành chỉ tiêu năm 2022 và kế hoạch các năm tiếp theo.<p>









