---
title: Lễ công bố quyết định bổ nhiệm Phó Tổng Giám đốc Công ty Dịch vụ Agribank
categories: hoạt động nội bộ
date: 2019-04-16 
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1555384195/samples/2019/bonhiemPT/bonhiemPT_preview.jpg
---

Ngày 16/4/2019, tại phòng họp Công ty Dịch vụ Agribank diễn ra Lễ công bố quyết định của Chủ tịch Hội đồng Thành viên Agribank về việc bổ nhiệm Phó Tổng Giám đốc Công ty Dịch vụ Agribank đối với ông Phạm Văn Thành.


Bí thư Đảng ủy,Chủ tịch - Tổng Giám đốc Công ty Dịch vụ Agribank ông Đỗ Đình Hồng đọc quyết định bổ nhiệm chức vụ Phó Tổng Giám đốc Công ty Dịch vụ Agribank đối với ông Phạm Văn Thành.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1555384194/samples/2019/bonhiemPT/bonhiemPT1.jpg)

*Ông Đỗ Đình Hồng – Chủ tịch, Tổng giám đốc đọc quyết định bổ nhiệm của Hội đồng thành viên Agribank*

Phát biểu tại buổi lễ, ông Phạm Văn Thành hứa trước ông Đỗ Đình Hồng và các lãnh đạo đơn vị trong Công ty sẽ cố gắng nhận, hoàn thành tốt nhiệm vụ trên cương vị mới để xứng đáng với truyền thống vai trò của Công ty trong hệ thống Agribank và ngành Ngân hàng.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1555384194/samples/2019/bonhiemPT/bonhiemPT2.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1555384195/samples/2019/bonhiemPT/bonhiemPT4.jpg)
  
*Ông Phạm Văn Thành - nhận nhiệm vụ và phát biểu tại buổi lễ*

Tại buổi lễ, ông Phạm Văn Thành cảm ơn sự quan tâm, tin tưởng của Hội đồng thành viên Agribank và mong muốn ông Đỗ Đình Hồng tạo điều kiện thuận lợi để học hỏi sớm tiếp cận công việc, nâng cao năng lực và hứa quyết tâm cùng Ban Lãnh đạo, toàn thể cán bộ, nhân viên Công ty sẽ kế thừa truyền thống, những kinh nghiệm, thành tích đã đạt được, góp phần tích cực vào sự phát triển bền vững của Công ty.


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1555384195/samples/2019/bonhiemPT/bonhiemPT3.jpg)
