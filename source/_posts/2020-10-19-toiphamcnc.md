---
title: Kiến thức, kỹ năng xử lý tình huống, chủ động phát hiện, ngăn chặn phòng ngừa tội phạm ngân hàng, lừa đảo chiếm đoạt tài sản sử dụng công nghệ cao.
date: 2020-10-20 
categories: hoạt động nội bộ
tacgia:
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1603167999/samples/2020/luadaocongnghecao/luadaocongnghecao_preview.jpg 

---
Kiến thức liên quan tới thủ đoạn lừa đảo chiếm đoạt tài sản sử dụng công nghệ cao

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1603167999/samples/2020/luadaocongnghecao/luadaocongnghecao1.jpg)

<p align="justify">Cuộc cách mạng Công nghiệp 4.0 dựa trên nền tảng công nghệ số và tích hợp tất cả các công nghệ thông minh để tối ưu hóa quy trình, phương thức sản xuất. Với lợi thế về công nghệ, cuộc CMCN 4.0 tác động sâu sắc đối với nền kinh tế và mọi lĩnh vực của đời sống xã hội của các nước.</p> 

<p align="justify">Công nhệ 4.0 mang lại nhiều lợi ích: tiết giảm nhân lực thủ công, giảm chi phí, nâng cao lợi nhuận... cung cấp những sản phẩm,dịch vụ tiện ích cho khách hàng; tuy nhiên cùng với đó là những nguy cơ, lỗ hổng, tạo điều kiện cho tội phạm công nghệ cao lợi dụng tiến hành các hoạt động vi phạm pháp luật.</p>


<p align="justify">Tội phạm sử dụng công nghệ cao đang có chiều hướng ngày càng phức tạp, gia tăng cả về số vụ và tính chất, mức độ, hậu quả, bởi chủ thể tội phạm thực hiện trên không gian mạng, có thể ở một nơi nhưng lại gây ra hậu quả toàn cầu, do đó rất khó xác định chủ thể để truy nguyên và bắt giữ, do rào cản về không gian mạng và lãnh thổ.</p> 


<p align="justify">Hiện nay tội phạm trong lĩnh vực cướp, lừa đảo chiếm đoạt tài sản sử dụng công nghệ cao đã được cảnh báo thông qua hàng loạt văn bản, bài viết của Nhà nước và các cơ quan ban ngành. Một số loại tội phạm công nghệ cao thường hoạt động thông qua các phương thức sau:</p>

<p align="justify">1.Tấn công máy tính, mạng máy tính: lợi dụng lỗ hổng bảo mật web, tấn công truy cập, lấy cắp, phá hoại dữ liệu phát tán virus, phần mềm gián điệp; tấn công từ chối dịch vụ.</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1603167999/samples/2020/luadaocongnghecao/luadaocongnghecao2.jpg)


<p align="justify">2.Chiếm đoạt tài sản: tội phạm lừa đảo, gian lận thẻ ngân hàng; chứng khoán, thương mại điện tử, thanh toán điện tử; lừa đảo bằng email, nickchat, tin nhắn SMS; gửi email, tin nhắn lừa đảo để lấy cắp account và password của email, nick chat… để lừa đảo, yêu cầu chuyển tiền, thẻ cào...</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1603167999/samples/2020/luadaocongnghecao/luadaocongnghecao3.jpg)

<p align="justify">Lừa đảo bằng hack e-mail mà nạn nhân thường là các công ty hoạt động mua bán và thanh toán tiền với các đối tác nước ngoài qua mạng Internet. Chúng tạo một địa chỉ e-mail gần giống với đối tác, sau đó làm giả e-mail giao dịch và yêu cầu nạn nhân thanh toán chuyển khoản vào tài khoản của đối tượng tại Việt Nam hoặc nước ngoài để chiếm đoạt.</p>


<p align="justify">3.Sử dụng công nghệ cao: quay lén, chụp ,nghe, quét để lấy trộm pass word, thông tin thẻ tín dụng, mã số két, mã số cổng... thực hiện hành vi ăn cắp, chiếm đoạt tài sản.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1603167999/samples/2020/luadaocongnghecao/luadaocongnghecao3.jpg)

*Bài viết có sử dụng một số hình ảnh và tư liệu trên mạng-Kỳ sau “Cảnh báo lừa đảo qua điện thoại ”.*

