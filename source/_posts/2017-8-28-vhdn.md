---
title: Công ty Dịch vụ Agribank tổ chức khóa học đào tạo văn hóa doanh nghiệp tại khu vực miền Bắc
date: 2017-8-28 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304213/samples/2017/vhdn/vhdn_preview.jpg

---

Trong hai ngày nghỉ thứ bảy và chủ nhật từ 26-27/8/2017 Công ty đã tổ chức khóa đào tạo chuyên sâu về văn hóa doanh nghiệp do Phó Giáo sư Tiến sỹ Nguyễn Văn Nhã, giảng viên Khoa quản trị kinh doanh- Đại học Quốc gia Hà Nội giảng dạy.

Khóa học được tổ chức nhằm tăng cường kiến thức về văn hóa doanh nghiệp cho cán bộ lãnh đạo, quản lý toàn Công ty qua đó góp phần nâng cao ý thức, phát huy giá trị văn hóa truyền thống của Agribank nói chung và của Công ty Dịch vụ Agribank nói riêng.
Các cán bộ tham dự đã sắp xếp công việc để  tham gia khóa học đầy đủ, đúng giờ với tinh thần hăng say nhiệt tình học hỏi, trau dồi cập nhật kiến thức mới. Chương trình đã nhấn mạnh đến bản sắc văn hóa cốt lõi của Agribank trên các góc độ:
 “Trung thực - Kỷ cương – Sáng tạo – Chất lượng – Hiệu quả”.


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304212/samples/2017/vhdn/vhdn1.jpg)
  
*Toàn cảnh lớp học khóa đào tạo chuyên sâu về văn hóa doanh nghiệp tại Công ty Dịch vụ Agribank.*

Khóa đào tạo còn đề cập đến các chuẩn mực, hành vi ứng xử giao tiếp của cán bộ công nhân viên chức Công ty. Đặc biệt các học viên đã sôi nổi trao đổi về cuộc cách mạng 4.0 đang diễn ra và định hướng văn hóa doanh nghiệp phù hợp với thời kỳ mới.
Văn hóa doanh nghiệp là chất keo kết dính các thành viên thành một khối thống nhất tạo nên sức mạnh của Công ty.Trong thời gian vừa qua Công ty Dịch vụ Agribank đã liên tục tổ chức học tập nâng cao nhận thức và quan tâm chăm sóc thể chất, tinh thần cho cán bộ công nhân viên thông qua các phong trào học tập và lao động./.


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304212/samples/2017/vhdn/vhdn2.jpg)






