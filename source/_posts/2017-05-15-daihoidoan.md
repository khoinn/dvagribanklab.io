---
title: Chúc mừng Đại hội Đoàn TNCS Hồ Chí Minh Công ty Dịch vụ Agribank nhiệm kỳ 2017 – 2019 thành công tốt đẹp
date: 2017-5-15 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304052/samples/2017/daihoidoan/daihoidoan_preview.jpg

---

Ngày 13/5/2017, Đoàn Thanh niên - Công ty Dịch vụ Agribank đã tổ chức Đại hội Đoàn TNCS Hồ Chí Minh nhiệm kỳ 2017 - 2019.
Đến tham dự Đại hội có đồng chí Đồng Đức Giang - Bí thư Đoàn TNCS Hồ Chí Minh Agribank.
Về phía Công ty Dịch vụ Agribank  có đồng chí Đỗ Đình Hồng Bí thư Đảng bộ, các đồng chí đại diện lãnh đạo các đơn vị cùng toàn thể đoàn viên thanh niên Công ty.
Thay mặt Ban chấp hành Đoàn Công ty dịch vụ Agribank nhiệm kỳ 2014 - 2017, đồng chí Trần Thu Thủy trình bày Báo cáo Tổng kết công tác Đoàn và phong trào thanh niên nhiệm kỳ 2014 – 2017, nêu những thành tích đạt được, đồng thời kiểm điểm, rút kinh nghiệm những tồn tại, hạn chế trong nhiệm kỳ qua và phương hướng, nhiệm vụ nhiệm kỳ 2017 - 2019.

Trong phần tham luận tại Đại hội các đồng chí đại biểu đã có những ý kiến đóng góp bổ ích về công tác tuyên truyền giáo dục chính trị tư tưởng, xây dựng tổ chức Đoàn. Triển khai phong trào thi đua tình nguyện xây dựng và bảo vệ Tổ quốc, chăm sóc giáo dục thanh niên, phân tích những nguyên nhân tồn tại và bài học kinh nghiệm cho nhiệm kỳ sau. Tham luận vào những chỉ tiêu thực hiện trên các mặt công tác của Đoàn đề ra các giải pháp, biện pháp thực hiện cho phù hợp trong nhiệm kỳ tới.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304052/samples/2017/daihoidoan/daihoidoan1.jpg)

*Đồng chí Trần Thu Thủy Ban chấp hành Đoàn Công ty dịch vụ Agribank Báo cáo Tổng kết công tác Đoàn.*

Phát biểu chỉ đạo tại Đại hội, đồng chí Đỗ Đình Hồng đã biểu dương sự nỗ lực cố gắng, hăng hái của tập thể Ban chấp hành Đoàn và đoàn viên trong việc thực hiện nhiệm vụ công tác chuyên môn cũng như công tác Đoàn, góp phần quan trọng cho thành tích chung của Công ty. Bên cạnh đó, đồng chí cũng chỉ ra những tồn tại, hạn chế cần phải khắc phục và yêu cầu Ban chấp hành nhiệm kỳ 2017 - 2019, đoàn viên thanh niên phải nỗ lực, cố gắng nhiều hơn nữa đáp ứng tốt yêu cầu, đòi hỏi trước tình hình và nhiệm vụ mới.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304052/samples/2017/daihoidoan/daihoidoan2.jpg)

*Đồng chí Đõ Đình Hồng - Bí thư Đảng ủy Công ty Dịch vụ Agribank phát biểu tại đại hội.*

Thay mặt Đoàn TNCS Hồ Chí Minh Agribank, đồng chí Đồng Đức Giang nhiệt liệt biểu dương các đồng chí đoàn viên Công ty Dịch vụ Agribank trong thời gian qua bằng sức trẻ, lòng hăng say là lực lượng tiên phong đi trước đã góp phần hoàn thành các chỉ tiêu công tác đoàn và phong trào thanh niên nhiệm kỳ qua. Đồng thời nhấn mạnh BCH Đoàn cần tăng cường công tác giáo dục chính trị, tư tưởng cho đoàn viên thanh niên. Đoàn viên phải phát huy năng lực sáng tạo, luôn xung kích trong mọi lĩnh vực hoạt động của đơn vị. Cần bám sát các Nghị quyết của cấp ủy Đảng để triển khai Nghị quyết của Đoàn tại các Chi Đoàn nơi đoàn viên công tác. Ngay sau Đại hội, Đoàn cơ sở phải xây dựng được quy chế hoạt động cụ thể của BCH. Đồng thời xây dựng chương trình thực hiện Nghị quyết Đại hội Đoàn nhiệm kỳ 2017 - 2019...

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304052/samples/2017/daihoidoan/daihoidoan3.jpg)

*Đồng chí Đồng Đức Giang - Bí thư Đoàn TNCS Hồ Chí Minh Agribank phát biểu tại đại hội.*

Với tinh thần dân chủ và sự đồng thuận cao, Đại hội đã bầu chọn Ban chấp hành Đoàn cơ sở  Công ty Dịch vụ Agribank, nhiệm kỳ 2017 - 2019 thành công tốt đẹp.



