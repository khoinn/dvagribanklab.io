---
title: Công ty Dịch vụ Agribank tuân thủ và thực hiện nghiêm túc các biện pháp phòng, chống dịch bệnh Covid-19, bảo đảm hoạt động sản xuất kinh doanh.
Date: 2021-08-04 16:32:35
categories: "hoạt động nội bộ"
tacgia: Phòng Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069872/samples/2021/phongchongdich/khukhuan_preview.jpg  

---

<p align="justify">Thực hiện các biện pháp phòng, chống dịch Covid-19 theo Chỉ thị của Thủ tướng Chính phủ, Ủy ban Nhân dân TP. Hà Nội, TP. Hồ Chí Minh và các cơ quan chức năng, với tinh thần “Chống dịch như chống giặc”, Công ty Dịch vụ Agribank đã và đang tiến hành các biện pháp cấp bách phòng chống dịch bảo đảm an toàn cho người lao động, khách hàng và duy trì hoạt động sản xuất kinh doanh.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan1.jpg)

<p align="justify">Đối với bộ phận điều vận, tiếp quỹ thuộc hai Trung tâm Dịch vụ Ngân quỹ là bộ phận có nguy cơ cao khi hàng ngày trực tiếp tiếp xúc, giao dịch với khách hàng, Công ty trang bị các phương tiện bảo hộ chống dịch như khẩu trang, kính chắn giọt bắn, găng tay..., đồng thời quán triệt cho cán bộ, nhân viên chấp hành nghiêm quy định “5K” của Bộ Y tế, đeo khẩu trang trong suốt quá trình làm việc, thực hiện giãn cách khi giao tiếp, rửa tay sát khuẩn thường xuyên... </p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan6.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan2.jpg)

<p align="justify">Việc phun khử khuẩn thường xuyên hàng ngày nhằm chủ động tiêu diệt mầm bệnh phát tán trong không khí và bám trên các bề mặt, góp phần hạn chế sự lây lan truyền bệnh của virus. Các nơi được tiến hành phun khử khuẩn hàng ngày gồm kho chứa hàng, khu vực văn phòng, nhà xưởng, hành lang, cầu thang, khu công cộng, các phương tiện chuyên dùng đi thu tiếp quỹ.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan3.jpg)

<p align="justify">Cán bộ, nhân viên và khách hàng đến Công ty làm việc đều được duy trì kiểm tra thân nhiệt, sát khuẩn tay; thông tin vào, ra của khách hàng được ghi chép lưu trữ để kịp thời khoanh vùng, truy vết khi có sự cố nhiễm bệnh xảy ra. Bên cạnh đó, trong thời gian thực hiện giãn cách xã hội, Công ty thực hiện làm việc luân phiên, trực tuyến; phân chia khung giờ ăn ca hợp lý; bố trí đầy đủ phương tiện, vật tư phòng, chống dịch theo quy định; xây dựng, ban hành các phương án ứng phó với tình hình dịch bệnh theo các cấp độ, chuẩn bị sẵn sàng các điều kiện để triển khai trong tình huống cấp bách.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan5.jpg)


<p align="justify">Ban lãnh đạo Công ty đã quán triệt tới từng đơn vị, người lao động nỗ lực tuân thủ các quy định về phòng chống dịch; đồng thời, động viên người lao động bình tĩnh, tin tưởng, phát huy tinh thần đoàn kết, chủ động, linh hoạt, sáng tạo để thực hiện mục tiêu “kép” vừa phòng chống dịch vừa duy trì ổn định, thúc đẩy hoạt động sản xuất kinh doanh, đảm bảo hoàn thành các chỉ tiêu kế hoạch năm 2021.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan7.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1628069873/samples/2021/phongchongdich/khukhuan8.jpg)


