---
title: Khuyến cáo chung của Bộ y tế về phòng chống bệnh viêm đường hô hấp cấp COVID-19(nCoV).
date: 2020-3-12
categories: hoạt động nội bộ
tacgia: Trích dẫn Cổng thông tin điện tử Bộ Y Tế
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705883/samples/2020/chongdich/covid_preview.jpg 
---

<p align="justify"> Triển khai công tác y tế năm 2020 và phòng chống dịch COVID-19 diễn ra 25/02/2020 </p>

<p align="justify"> ... “ Đồng chí Phó thủ tướng Chính phủ Vũ Đức Đam cũng đã tri ân những người thầy thuốc nhân kỷ niệm 65 năm Ngày Thầy thuốc Việt Nam 27-02. Đồng thời biểu dương, ghi nhận những kết quả Ngành Y tế đạt được cũng như công tác phòng, chống dịch bệnh COVID -19 trong thời gian qua.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705883/samples/2020/chongdich/covid_preview.jpg)

_Đồng chí Vũ Đức Đam, Ủy viên Ban chấp hành Trung ương Đảng, Phó Thủ tướng Chính phủ, Bí thư Ban Cán sự Đảng Bộ Y tế phát biểu chỉ đạo Hội nghị_

<p align="justify">Phó Thủ tướng đề nghị Ngành Y tế trong thời gian tới cần thực hiện tốt công tác chăm sóc sức khỏe cho Nhân dân; trước diễn biến dịch bệnh COVID -19 trên thế giới đang diễn biến phức tạp, không được chủ quan, cần tích cực tuyên truyền người dân nâng cao nhận thức trong công tác phòng chống dịch. Các địa phương chủ động tăng cường các biện pháp cách ly đối với người bị nghi nhiễm và các biện pháp phòng chống dịch thiết thực, hiệu quả.../.”</p> 


<p align="justify">Khuyến cáo chung của Bộ y tế về phòng chống bệnh viêm đường hô hấp cấp COVID-19(nCoV).</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705884/samples/2020/chongdich/covid1.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705884/samples/2020/chongdich/covid2.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705884/samples/2020/chongdich/covid3.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705884/samples/2020/chongdich/covid4.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1582705884/samples/2020/chongdich/covid5.jpg)
