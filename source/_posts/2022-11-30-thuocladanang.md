---
title: CÔNG TY DỊCH VỤ AGRIBANK TĂNG CƯỜNG MỞ RỘNG QUAN HỆ HỢP TÁC, PHÁT TRIỂN THỊ TRƯỜNG
categories: hoạt động nội bộ
date: 2022-11-30
tacgia: Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1669883657/samples/2022/thuocladanang/THUOCLADANANG1_PREVIEW.jpg

---

<p align="justify"> Ngày 30/11/2022, Công ty Dịch vụ Agribank do Ông Trần Duy Hưng - Chủ tịch Công ty làm đại diện đã đến thăm và làm việc tại Công ty TNHH Thuốc lá Đà Nẵng. Cùng tham gia buổi làm việc có Ông Lương Văn Dũng, Giám đốc Nhà in Ngân hàng II trực thuộc Công ty Dịch vụ Agribank. Về phía Nhà máy có Ông Nguyễn Văn Long - Giám đốc Công ty cùng một số cán bộ quản lý đã nhiệt tình đón tiếp đoàn.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1669883657/samples/2022/thuocladanang/THUOCLADANANG1.jpg)

<p align="justify">Trong quá trình tham quan và làm việc thực tế tại Nhà máy, đại diện lãnh đạo hai bên đơn vị đã có những trao đổi thiết thực về nhu cầu thị trường và tình hình sản xuất kinh doanh tại khu vực, qua đó nắm bắt cơ hội để mở ra quan hệ hợp tác đôi bên trong tương lai. Hai bên cũng bày tỏ vui mừng và đánh giá cao những nỗ lực, sự phát triển của cả đơn vị trong những năm vừa qua.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1669883657/samples/2022/thuocladanang/THUOCLADANANG2.jpg)

<p align="justify">Chuyến thăm lần này đã mở rộng quan hệ hợp tác giữa Công ty Dịch vụ Agribank và Công ty TNHH Thuốc lá Đà Nẵng. Đây cũng là tiền đề cho việc triển khai kế hoạch nghiên cứu thị trường và phát triển thị phần của Công ty những năm tiếp theo nhằm thực hiện mục tiêu và định hướng phát triển của Ban lãnh đạo Công ty hướng tới đẩy mạnh phát triển lĩnh vực dịch vụ, trong đó ưu tiên cho hoạt động in thương mại./.<p>

