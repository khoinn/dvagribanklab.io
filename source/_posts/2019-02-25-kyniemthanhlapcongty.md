---
title: Công ty Dịch vụ Agribank tổ chức kỷ niệm 15 năm thành lập (20/02/2004 - 20/02/2019) và triển khai nhiệm vụ sxkd năm 2019
Date: 2018-02-25 3:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu_preview.jpg

---

<p align="justify">Ngày 23/2/2019, Công ty TNHH MTV Dịch vụ Ngân hàng Nông nghiệp Việt Nam đã tổ chức nội bộ Kỷ niệm 15 năm thành lập và động viên khích lệ, trao thưởng cho những tập thể cá nhân có thành tích xuất sắc năm 2018, triển khai nhiệm vụ sản xuất kinh doanh năm 2019. Buổi lễ có đại diện CBCNV hai miền Nam - Bắc gặp mặt tại hội trường Công ty số 10 Chùa Bộc, Đống Đa, Hà Nội trong không khí làm việc tích cực ngay từ đầu năm. </p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu1.jpg)
*Đồng chí Đỗ Đình Hồng - Bí thư, Chủ tịch, Tổng Giám đốc Công ty khai mạc buổi lễ*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu2.jpg)
*Toàn cảnh buổi Lễ kỷ niệm và triển khai nhiệm vụ sxkd năm 2019*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu3.jpg)
*Chương trình văn nghệ chào mừng của Đoàn Thanh niên Công ty*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu4.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu5.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu6.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu7.jpg)
*Đồng chí Dương Văn Ba - Nguyên Phó Tổng Giám đốc phát biểu tại buổi lễ*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu8.jpg)
*Đồng chí Trịnh Quốc Phong - Nguyên Phó Tổng Giám đốc phát biểu tại buổi lễ*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu9.jpg)
*Trao thưởng cho các tập thể có thành tích tốt*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu10.jpg)
*Trao thưởng cho các cá nhân có thành tích xuất sắc*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu11.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu12.jpg)


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu13.jpg)
*Hình ảnh thắp nến bánh gato trong lễ kỷ niệm*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1551062370/samples/2019/nhiemvu/nhiemvu14.jpg)
*Hình ảnh mở sâm-panh khai tiệc*


<p align="justify">Trong quá trình tiếp nối và phát triển 15 năm qua, Công ty TNHH MTV Dịch vụ Ngân hàng Nông nghiệp Việt Nam luôn nhận được sự quan tâm của Ngân hàng Nhà nước và Ngân hàng No&PTNT Việt Nam, các đơn vị trong hệ thống, các đối tác, những người bạn thân thiết cùng với đội ngũ CBCNV công ty đã cùng nhau tạo lập nên sự thành công của Công ty ngày hôm nay. Đ/c Đỗ Đình Hồng - Bí thư, Chủ tịch, Tổng giám đốc thay mặt cho Công ty TNHH MTV Dịch vụ Ngân hàng Nông nghiệp Việt Nam đã gửi lời cảm ơn chân thành nhất tới toàn thể CBCNV, những người bạn, những người đã cùng đồng hành với Công ty trong thời gian qua.</p>