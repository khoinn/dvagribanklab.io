---
title: Công ty Dịch vụ Agribank chia sẻ khó khăn với đồng bào vùng lũ Miền Trung
date: 2016-10-30 
categories: "hoạt động nội bộ"
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304614/samples/tuthienhatinhquangbinh/qb_preview.jpg
---

Từ ngày 27 đến ngày 30 tháng 10 năm 2016, Công đoàn Công ty Dịch vụ Agribank, được sự thống nhất của Đảng ủy, ban Tổng Giám đốc. Đồng chí Lê Minh Quang Phó Tổng Giám đốc, Chủ tịch Công đoàn dẫn đoàn chuyển khẩn cấp 4 tấn gạo và 20 triệu đồng tới đồng bào vùng lũ miền Trung, được trích từ nguồn quỹ từ thiện của Công đoàn
Ngày 27/10/2016 đoàn đã tới xã Hương Xuân, huyện Hương Khê, tỉnh Hà Tĩnh, vùng rốn lũ, nơi chịu thiệt hại nặng nề nhất trong cơn lũ vừa qua. Trước cảnh người dân vùng lũ lụt miền Trung đang phải gồng mình chống trọi với những hậu quả mà thiên tai để lại, một khung cảnh xơ xác tiêu điều hiện ra trước mắt, nước đã rút nhưng những cành cây, rơm rạ vẫn còn mắc lại trên đường dây điện

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304612/samples/tuthienhatinhquangbinh/ht1.jpg)

*Công ty Dịch vụ Agribank đến với đồng bào vùng lũ*

Công ty Dịch vụ Agribank cùng Agribank chi nhánh Hương Khê Hà Tĩnh trao tận tay 101 suất quà cho người dân

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304612/samples/tuthienhatinhquangbinh/ht2.jpg)
  
*Ông Phạm Ngọc Ruyển trao quà cho người dân vùng lũ Hương Khê, Hà Tĩnh*

Đặc biệt đoàn đến tận nhà bà mẹ Việt Nam anh hùng Ngô Thị Ý năm nay đã 91 tuổi xóm Phú Hương 2, xã Hương Xuân động viên chia sẻ khó khăn cùng bà con.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304612/samples/tuthienhatinhquangbinh/ht3.jpg)

*Ông Lê Minh Quang cùng đoàn đến trao quà tại nhà cho Bà Mẹ Việt Nam Anh hùng*


Và tặng quà cho các cháu nhỏ. Đoàn đã khẩn trương làm việc đến 7 giờ tối để cố gắng chuyển toàn bộ phần quà tới người dân, để phần nào giúp người dân nơi đây giảm bớt những khó khăn.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304612/samples/tuthienhatinhquangbinh/ht4.jpg)

*Đoàn trao quà cho các cháu vùng lũ Hương Xuân*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304612/samples/tuthienhatinhquangbinh/ht5.jpg)

*Đoàn trao quà cho đồng bào vùng lũ*

Ngày hôm sau đoàn đến xã Đức Hoá, huyện Tuyên Hoá, Tỉnh Quảng Bình là địa phương thiệt hại nặng nề do trận bão lũ vừa qua. Nơi đây đã bị ngập lụt diện rộng làm cho 830 hộ gia đình chìm sâu trong nước biển, 46 hộ gia đình bị lũ quét, 11 hộ bị sập tường, trôi nhà và hơn 1500 gia cầm bị nước cuốn trôi. Hệ thống cơ sở hạ tầng, trường học, giao thông, thuỷ lợi bị thiệt hại nặng nề ước tính lên tới 15 tỷ đồng. Tuy nhiên Đảng uỷ và Chính quyền xã đã chủ động di rời các hộ dân vì thế trên địa bàn xã không có thiệt hại về người nhưng đã thiệt hại nặng nề về tài sản

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304614/samples/tuthienhatinhquangbinh/qb1.jpg)

*Công ty Dịch vụ Agribank cùng Agribank Tuyên Hóa- Quảng Bình trao quà cho bà con*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304614/samples/tuthienhatinhquangbinh/qb2.jpg)

*Hình ảnh trao quà cho bà conTuyên Hóa, Quảng Bình*


Nhận thức đây chính là thời điểm rất cần sự hỗ trợ của các tổ chức, cá nhân cả về tinh thần và vật chất nhằm khắc phục hậu quả, ổn định đời sống cho nhân dân vùng lũ lụt. Công ty Dịch vụ Agribank với truyền thống đoàn kết, tinh thần tương thân tương ái giúp đỡ nhau trong hoạn nạn khó khăn, kết nối đồng hành cùng chi nhánh Agribank Hà Tĩnh, Quảng Bình đã chia sẻ động viên với tinh thần "Một miếng khi đói bằng một gói khi no"… mong rằng miền Trung thương yêu sẽ nhận được nhiều sự ủng hộ của các tổ chức, cá nhân khác để người dân vượt qua khó khăn












