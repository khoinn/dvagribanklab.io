---
title: Công ty Dịch vụ Agribank hoạt động tri ân nhân ngày thương binh liệt sỹ ngày 27-7
date: 2017-7-27 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304092/samples/2017/phucuong/phucuong_preview.jpg

---

Tiếp nối truyền thống “Đền ơn đáp nghĩa“, Công ty Dịch vụ Agribank tích cực tổ chức các hoạt động hướng tới kỷ niệm 70 năm ngày thương binh liệt sỹ 27/7/1947- 27/7/2017. Ghi nhớ công lao to lớn của các anh hung liệt sỹ, hòa chung không khí kỷ niệm ngày lễ trọng đại của đất nước, Công ty Dịch vụ Agribank tham gia chương trình tu bổ nghĩa trang liệt sỹ, công đức một Bia tưởng niệm tại xã Phú Cường, huyện Sóc Sơn, TP Hà Nội.

Tham dự buổi lễ khánh thành Bia tưởng niệm anh hùng liệt sĩ xã Phú Cường có đồng chí Đỗ Đình Hồng - Bí thư Đảng ủy, Chủ tịch kiêm Tổng Giám đốc Công ty, đồng chí Trần Văn Mưu - Phó Chủ tịch xã Phú Cường cùng đại diện công đoàn, đoàn thanh niên, phòng ban Công ty. Trong không khí trang nghiêm, đồng chí Đỗ Đình Hồng đã bày tỏ lời tri ân sâu sắc tới các anh hùng liệt sỹ, các thương binh đã hy sinh xương máu vì sự nghiệp cao cả của Đảng, vì độc lập tự do của Tổ quốc và khẳng định thế hệ hôm nay sẽ tiếp bước cha anh không ngừng lao động, rèn luyện, cống hiến hết sức mình vì sự nghiệp dân giàu, nước mạnh, xã hội dân chủ, công bằng, văn minh.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304088/samples/2017/phucuong/pc1.jpg)
  
*Đoàn Công ty Dịch vụ Agribank đặt vòng hoa viếng tại nghĩa trang liệt sỹ._*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304088/samples/2017/phucuong/pc2.jpg)

*Đoàn Công ty Dịch vụ Agribank thắp hương tại nghĩa trang liệt sỹ.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304088/samples/2017/phucuong/pc3.jpg)

*Ông: Đỗ Đình Hồng phát biểu tại nghĩa trang Liệt sỹ.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304088/samples/2017/phucuong/pc4.jpg)

*Lãnh đạo Công ty và Xã tại buổi lễ khánh thành.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304088/samples/2017/phucuong/pc5.jpg)

*CBCNV Công ty Dịch vụ Agribank thắp hương tri ân các Liệt sỹ.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304088/samples/2017/phucuong/pc6.jpg)




