---
title: ĐẠI DIỆN CÔNG TY LOUISENTHAL ĐẾN THĂM VÀ LÀM VIỆC TẠI CÔNG TY DỊCH VỤ AGRIBANK

categories: hoạt động nội bộ
date: 2022-10-28
tacgia: Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1666929094/samples/2022/ctyduc/congtyduc1_preview.jpg

---

<p align="justify">Sáng ngày 28/10/2022, Công ty Dịch vụ Agribank đã vinh dự đón tiếp đại diện Công ty Louisenthal - Tiến sĩ Thomas Brandstätter, Giám đốc Kinh doanh khu vực Châu Á trong không khí thân mật, trang trọng và cởi mở. Chuyến thăm được xây dựng trên tinh thần giao lưu, củng cố mối quan hệ giữa hai bên đồng thời mở rộng thêm những cơ hội kinh doanh và hợp tác trong tương lai.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1666929094/samples/2022/ctyduc/congtyduc1.jpg)

*Ông Trần Duy Hưng - Chủ tịch Công ty nhận món quà lưu niệm từ Tiến sĩ Thomas Brandstätter - Giám đốc Kinh doanh Công ty Louisenthal khu vực Châu Á*

Đón tiếp đại diện của Công ty Louisenthal tại văn phòng, Ông Trần Duy Hưng - Chủ tịch Công ty Dịch vụ Agribank chia sẻ: “Chúng tôi rất vinh dự được đón tiếp Công ty Louisenthal và mong muốn trong tương lai sẽ thắt chặt hơn nữa mối quan hệ hợp tác hiệu quả, bền vững giữa hai bên. Chúng tôi tin tưởng sự hợp tác và đồng hành dựa trên thế mạnh của mỗi Bên chắc chắn sẽ gặt hái được nhiều thành quả trong tương lai, đóng góp thiết thực vào mục tiêu chiến lược kinh doanh của mỗi doanh nghiệp”.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1666929094/samples/2022/ctyduc/congtyduc2.jpg)

Louisenthal có bề dày lịch sử hơn 200 năm, bắt đầu vào năm 1818 từ xưởng làm đồng hồ của Jakob Deisenrieder và trở thành công ty con trực thuộc Tập đoàn công nghệ Giesecke + Devrient vào năm 1964. Hiện nay Louisenthal là một trong những nhà sản xuất giấy in bảo an hàng đầu thế giới, là nhà cung cấp chính giấy đặc biệt cho Ngân hàng Trung ương Châu Âu và khách hàng tại hơn 100 quốc gia trên toàn thế giới. 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1666929094/samples/2022/ctyduc/congtyduc3.jpg)

*Ông Trần Duy Hưng - Chủ tịch Công ty trao tặng quà lưu niệm tới Tiến sĩ Thomas Brandstätter - Giám đốc Kinh doanh Công ty Louisenthal khu vực Châu Á*

Trong khi đó, bên cạnh hoạt động dịch vụ ngân quỹ, với quy trình quản lý theo tiêu chuẩn chất lượng, an toàn cao cùng đội ngũ cán bộ giàu kinh nghiệm và lực lượng công nhân lành nghề, Công ty Dịch vụ Agribank đã tạo dựng được thương hiệu riêng về lĩnh vực in bảo mật với mặt hàng truyền thống, mũi nhọn như các sản phẩm in cao cấp, ấn chỉ quan trọng có giá, có số... cho hệ thống tài chính ngân hàng và các tổ chức, doanh nghiệp. Hiện nay, Công ty là đối tác tin cậy của hơn 40 ngân hàng, tổ chức tín dụng trên toàn quốc.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1666929094/samples/2022/ctyduc/congtyduc4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1666929094/samples/2022/ctyduc/congtyduc5.jpg)

Buổi gặp gỡ đã đặt nền móng cho sự hợp tác bền vững trên cơ sở lợi thế của hai Bên để khai thác tốt nhất nguồn lực và tiềm năng của mình, từ đó nâng cao hiệu quả kinh doanh và sức cạnh tranh trên thị trường./.








