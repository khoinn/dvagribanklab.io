---
title: Lãnh đạo Ngân hàng Nhà nước Việt Nam đến thăm và làm việc với Công ty Dịch vụ Agribank
date: 2021-12-24
categories: "hoạt động nội bộ"
tacgia: Phòng Kinh doanh-Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1641810095/samples/2021/PT%C4%90/ptd_preview.jpg 

---

<p align="justify">Trong không khí hứng khởi, nỗ lực quyết tâm hoàn thành kế hoạch của những ngày cuối năm 2021, chiều ngày 24/12/2021, Công ty Dịch vụ Agribank vinh dự được đón tiếp các đồng chí là lãnh đạo của Ngân hàng Nhà nước Việt Nam (Ngân hàng Nhà nước), Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam (Agribank) đến khảo sát, làm việc và chỉ đạo triển khai thực hiện kế hoạch in sản phẩm đặc biệt tại Công ty Dịch vụ Agribank giai đoạn 2022-2026.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1641807249/samples/2021/PT%C4%90/4.jpg)
*Phó Thống đốc thường trực Ngân hàng Nhà nước - Đào Minh Tú đến thăm và làm việc tại Công ty Dịch vụ Agribank*

<p align="justify">Về phía Ngân hàng Nhà nước có sự hiện diện của Ông Đào Minh Tú - Phó Bí thư Ban Cán sự Đảng, Phó Thống đốc Thường trực Ngân hàng Nhà nước cùng đại diện lãnh đạo và cán bộ các đơn vị thuộc Ngân hàng Nhà nước (Cục Phát hành và Kho quỹ, Vụ Kiểm toán nội bộ, Vụ Tài chính - Kế toán). Về phía đại diện của Agribank có Ông Phạm Đức Ấn - Bí thư Đảng ủy, Chủ tịch Hội đồng Thành viên Agribank, Ông Nguyễn Minh Phương - Phó Tổng Giám đốc Agribank cùng lãnh đạo các Ban liên quan tham dự họp.<p> 
Trước khi tiến hành hội nghị, đoàn công tác của Ngân hàng Nhà nước đã đi thăm, khảo sát cơ sở vật chất, khu vực sản xuất, nhà xưởng, kho tàng, tình hình máy móc, trang thiết bị, công tác đảm bảo an ninh, an toàn của Công ty. Lãnh đạo Ngân hàng Nhà nước đã đánh giá cao cơ sở vật chất, các hệ thống giám sát cảnh báo chống đột nhập, hệ thống phòng cháy chữa cháy, các lớp bảo vệ mục tiêu do con người đảm nhiệm từ vòng ngoài cho đến nơi nhà xưởng đặt máy móc vận hành sản xuất sản phẩm, bảo đảm an ninh an toàn nghiêm ngặt, đáp ứng các tiêu chuẩn theo quy định.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1641807249/samples/2021/PT%C4%90/11.jpg)
*Đồng chí Nguyễn Hải Long - Phó Tổng Giám đốc Agribank, Chủ tịch Công ty Dịch vụ Agribank báo cáo tại Hội nghị*

<p align="justify">Tại buổi làm việc, Ông Nguyễn Hải Long - Phó Tổng Giám đốc Agribank, Bí thư Đảng uỷ, Chủ tịch Công ty Dịch vụ Agribank đã báo cáo kết quả thực hiện nhiệm vụ chính trị giai đoạn 2017-2021 và công tác chuẩn bị cho kế hoạch giai đoạn 2022-2026. Báo cáo nêu lên những thuận lợi, khó khăn của Công ty trong giai đoạn qua, đặc biệt trong hai năm 2020-2021, diễn biến của tình hình dịch bệnh Covid-19 làm đứt gẫy các chuỗi cung ứng dẫn đến rủi ro về sụt giảm khối lượng hàng hóa, dịch vụ; sự chậm trễ, thiếu hụt, gia tăng chi phí nguyên vật liệu đầu vào cũng như sự gián đoạn hoạt động sản xuất kinh doanh.
Tuy nhiên, với sự ủng hộ và tin tưởng của Ngân hàng Nhà nước cùng với trách nhiệm, tinh thần quyết liệt, sự linh hoạt, sáng tạo của Ban lãnh đạo và tập thể người lao động, Công ty Dịch vụ Agribank đã hoàn thành chỉ tiêu kế hoạch Ngân hàng Nhà nước giao từng năm trong giai đoạn 2017-2021, đảm bảo an toàn, chất lượng sản phẩm với tỷ lệ sai hỏng thấp, góp phần tiết kiệm vật tư và duy trì giá thành sản phẩm ở mức bằng hoặc thấp hơn Nhà máy In tiền Quốc gia.

Sau khi nghe ý kiến phát biểu của các đồng chí lãnh đạo Agribank, lãnh đạo các Vụ, Cục NHNN và lãnh đạo Công ty Dịch vụ Agribank tham dự họp, Phó thống đốc thường trực NHNN Đào Minh Tú đã phát biểu kết luận buổi làm việc, ghi nhận những kết quả Công ty đã đạt được trong thời gian qua. Đặc biệt trong tình hình dịch bệnh rất phức tạp nhưng Công ty đã nỗ lực hoàn thành các mục tiêu và kế hoạch Ngân hàng Nhà nước giao. Phó thống đốc NHNN đề nghị trong thời gian tới, Công ty cần tiếp tục duy trì thực hiện tốt việc bảo đảm tuyệt đối an toàn trong công tác in sản phẩm đặc biệt. Chú trọng đến chất lượng sản phẩm, đáp ứng đúng yêu cầu tiến độ giao hàng. Nghiên cứu, thực hiện các giải pháp tiết giảm chi phí in ấn (từ khâu mua nguyên vật liệu, các công đoạn sản xuất, giảm tỷ lệ sản phẩm hỏng,…), qua đó giúp tiết kiệm ngân sách Nhà nước.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1641807249/samples/2021/PT%C4%90/13.jpg)
*Đồng chí Phạm Đức Ấn - Chủ tịch Hội đồng Thành viên Agribank phát biểu tại Hội nghị*

Với đặc thù là Công ty có truyền thống và thế mạnh in các sản phẩm có số có giá, Phó thống đốc NHNN nhấn mạnh tầm quan trọng của việc đầu tư trang thiết bị hiện đại, ứng dụng khoa học công nghệ trong sản xuất để nâng cao chất lượng sản phẩm, đạt hiệu suất cao. Về định hướng phát triển trong thời gian tới, Phó thống đốc NHNN yêu cầu Công ty không ngừng đổi mới, cải tiến nhằm nâng cao hiệu quả quản lý đối với đội ngũ lãnh đạo của Công ty. Khai thác hiệu quả năng lực hiện có, tiếp tục phát triển hoạt động in thương mại (đẩy mạnh tiếp thị, tìm kiếm, đa dạng khách hàng trong và ngoài hệ thống Agribank….); quan tâm, nâng cao đời sống cán bộ, công nhân của Công ty. Tập trung nghiên cứu để triển khai các dịch vụ phù hợp với năng lực, đồng thời tận dụng được nguồn lực sẵn có của Công ty như: dịch vụ tiêu huỷ tiền, cho thuê két sắt, vận chuyển tiền, mở rộng in ấn sản phẩm thương mại, cho thuê phương tiện, dịch vụ lưu trú… 
Đồng chí Phó thống đốc NHNN cũng đặt ra yêu cầu đối với Agribank, trong vai trò là đơn vị chủ sở hữu của Công ty tiếp tục quan tâm chỉ đạo, tăng cường công tác quản lý, giám sát, đầu tư máy móc thiết bị, cơ sở vật chất, hỗ trợ Công ty về nguồn vốn, nhân lực, tăng cường năng lực tổ chức quản lý, điều hành sản xuất, tạo điều kiện thuận lợi cho Công ty Dịch vụ Agribank hoạt động ổn định, phát triển hướng đến mục tiêu hoàn thành nhiệm vụ chính trị Ngân hàng Nhà nước giao.

Thay mặt Công ty, đồng chí Chủ tịch Công ty Nguyễn Hải Long đã phát biểu lĩnh hội, tiếp thu ý kiến chỉ đạo của Phó thống đốc thường trực NHNN. Buổi làm việc đã kết thúc tốt đẹp và là dấu mốc quan trọng, mở ra hướng phát triển chiến lược cho hoạt động của Công ty Dịch vụ Agribank trong giai đoạn 05 năm tiếp theo 2022-2026 hướng đến mục tiêu phát triển vững vàng, ổn định, đảm bảo hoàn thành nhiệm vụ chính trị được Ngân hàng Nhà nước giao.













  







