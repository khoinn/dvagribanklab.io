---
title: Kiến thức, kỹ năng xử lý tình huống, chủ động phát hiện, ngăn chặn phòng ngừa tội phạm ngân hàng, lừa đảo chiếm đoạt tài sản sử dụng công nghệ cao (tiếp theo kỳ trước).
date: 2020-10-30 
categories: hoạt động nội bộ
tacgia:
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_preview.jpg  

---
<p align="justify"> “Kiến thức liên quan tới thủ đoạn lừa đảo chiếm đoạt tài sản sử dụng công nghệ cao - Lừa đảo qua điện thoại”.

KHUYẾN CÁO! "Tuyệt đối không truy cập đường link lạ được gửi qua điện thoại, email hoặc cung cấp thông tin ngân hàng cho người lạ để tránh bị mất tiền oan"!</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_1.jpg)


<p align="justify"> 5. Lừa đảo qua điện thoại: Hiện nay có hiện tượng kẻ gian mạo danh ngân hàng, người thân, bạn bè gửi đường link giả mạo website của ngân hàng hoặc tổ chức chuyển tiền tới khách hàng và yêu cầu khách hàng cung cấp  các thông tin cá nhân, số tài khoản, số điện thoại, mật khẩu...để thực hiện giao dịch trên internet, gây thiệt hại cho khách hàng.</p>


<p align="justify">Thủ đoạn của kẻ lừa đảo là gửi tin nhắn qua điện thoại hoặc thư điện tử của nạn nhân. Nội dung chứa liên kết (link) giả mạo trang web hay giao diện các ngân hàng, tổ chức tài chính. Chúng dụ nạn nhân truy cập đường link, sau đó điền các thông tin liên quan để đăng nhập như đăng nhập trên trang web chính chủ hoặc ứng dụng ngân hàng trên điện thoại. Sau khi có được thông tin cá nhân của chủ tài khoản, kẻ gian kiểm soát được tài khoản chuyển tiền trực tuyến của khách hàng và thực hiện hành vi lừa đảo, đánh cắp tiền trong tài khoản.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_2.jpg)

<p align="justify"> - Vụ việc điển hình mới đây; Công an Đắk Nông điều tra vụ chị Lê Thị Thu Thảo (vợ của một công nhân tử nạn trong sự cố Rào Trăng 3) bị kẻ gian gọi điện thoại, lừa đảo chiếm đoạt 100 triệu đồng. Đây là số tiền các nhà hảo tâm ủng hộ, giúp đỡ gia đình chị Thảo. Theo trình báo của nạn nhân, chiều 20/10, một người lạ gọi điện nói muốn chuyển 6 triệu để động viên gia đình sau biến cố. Người này cho biết tiền chuyển từ tài khoản quốc tế nên gửi tin nhắn chứa liên kết đến điện thoại của chị Thảo. Nếu nhập các thông tin theo chỉ dẫn, chị sẽ nhận được tiền.Tuy nhiên, sau khi người phụ nữ nhập số tài khoản và mật khẩu dịch vụ ngân hàng trên trang web trong tin nhắn, nạn nhân phát hiện tài khoản bị mất 100 triệu đồng.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_3.jpg)

<p align="justify"> - Kẻ gian mạo danh nhân viên của ví điện tử, ngân hàng… để gọi điện, nhắn tin thông báo khách hàng trúng thưởng nhằm đánh cắp mã OTP, đánh cắp tiền trong tài khoản. Ví điện tử MoMo vừa cảnh báo thủ đoạn kẻ gian giả danh nhân viên của MoMo gọi điện, nhắn tin từ số điện thoại lạ thông báo trúng thưởng lớn, rồi yêu cầu người dùng hoàn tất thủ tục nhận thưởng bằng cách cung cấp các thông tin bảo mật của tài khoản (mật khẩu đăng nhập, mã xác thực (OTP), số thẻ ngân hàng) "để đối chiếu thông tin".</p> 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_4.jpg)


<p align="justify"> - Thủ đoạn phổ biến gần đây là lừa khách hàng mở thẻ tín dụng không lãi suất và thu phí phát hành thẻ hoặc quảng cáo nhận hồ sơ mở thẻ tín dụng qua các trang mạng, ứng dụng mạng xã hội (Facebook, Zalo) rồi thu phí người dùng…
Gần đây có một số đối tượng giả danh cán bộ, nhân viên Ngân hàng TMCP Sài Gòn (SCB) gọi điện thoại, nhắn tin, email tư vấn khách hàng mở thẻ tín dụng, thẻ khách hàng thân thiết hạn mức 30 triệu đồng, miễn lãi suất trong 3 năm… Khách hàng chỉ cần thanh toán phí 300.000 đồng rồi cung cấp, xác nhận thông tin qua điện thoại; thẻ sẽ được giao tận nhà qua đường bưu điện. Sau khi đóng phí, nhận thẻ xong nhưng không sử dụng được, khách hàng mới biết bị lừa.</p>
 
<p align="justify">Thủ đoạn này được nhiều ngân hàng liên tục cảnh báo trong những ngày gần đây. Tuy nhiên, do một số khách hàng ít quan tâm, bỏ sót thông tin nên mới bị kẻ gian lừa đảo, chiếm đoạt tài sản.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_5.jpg)

<p align="justify"> - Sử dụng phần mềm công nghệ cao (Voice over IP) giả số điện thoại cơ quan Công an để lừa đảo chiếm đoạt tài sản: Đối tượng xấu dùng ứng dụng công nghệ phần mềm công nghệ cao Voice over IP (cách gọi sử dụng ứng dụng truyền tải giọng nói qua mạng máy tính, giả số điện thoại hiển thị trên màn hình,…) thực hiện các cuộc gọi đến có số điện thoại hiển thị trên màn hình điện thoại người nhận các số giống với số Trực ban Công an,… Sau đó tự xưng cán bộ Công an đe dọa, tống tiền nhân dân. Số điện thoại lừa đảo sẽ xuất hiện thêm các đầu số: 1080, +084028 hoặc +028,…  phía trước các đầu số máy giả mạo hiển thị khi gọi đến.</p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1604030533/samples/2020/luadaocongnghecao/ldcnphan2_6.jpg)

<p align="justify"> Các đối tượng lừa đảo gọi điện thoại cho nạn nhân tự xưng danh là đại diện cho cơ quan pháp luật (công an, tòa án, viện kiểm soát) thông báo: Nợ tiền cước điện thoại, tiền điện, tiền nước, liên quan đến một vụ án, rửa tiền,… các đối tượng dùng lời lẽ dọa nạt yêu cầu nạn nhân phải hợp tác phục vụ yêu cầu điều tra, nếu không hợp tác và làm theo hướng dẫn sẽ bị xử lý hình sự, đồng thời yêu cầu nạn nhân phải giữ bí mật, không được báo cho người nhà biết. Chúng dò hỏi thông tin cá nhân, số tài khoản, có những tài khoản gửi ở đâu và yêu cầu chuyển vào tài khoản cho “cơ quan pháp luật” kiểm tra, sẽ trả lại trong vài giờ. Sau đó, chúng cung cấp số tài khoản để chiếm đoạt tiền của nạn nhân gửi...</p>


*Bài viết có sử dụng một số hình ảnh và tư liệu trên mạng .
Kỳ sau “Kiến thức liên quan tới thủ đoạn lừa đảo chiếm đoạt tài sản sử dụng công nghệ cao - Lưà đảo bằng các hình thức khác”*

