---
title: Đại hội Đảng bộ nhiệm kỳ 2015-2020 công ty Dịch vụ Agribank thành công rực rỡ
date: 2015-09-12
categories : hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304479/samples/daihoidang/dh_preview.jpg

---

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304477/samples/daihoidang/dh2.jpg)

ĐC Nguyễn Danh Vận phó chủ nhiệm UBKT Đảng ủy NHNo&PTNT VIệt Nam tới dự Đại hội

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304477/samples/daihoidang/dh3.jpg)
Đ/C Đỗ Đình Hồng Bí Thư Đảng ủy, Chủ tịch HĐTV phát biểu tại hội nghị.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304477/samples/daihoidang/dh4.jpg)

Ông Trịnh Quốc Phong - Chủ nhiệm ủy ban kiểm tra Đảng ủy phát biểu tại Đại hội Đảng bộ Công ty tháng 6/2015 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304477/samples/daihoidang/dh5.jpg)

Chủ tịch công đoàn - Ông Lê Minh Quang phát biểu tại Đại hội Đảng bộ Công ty tháng 6/2015 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304477/samples/daihoidang/dh6.jpg)

Đồng chí Nguyễn Văn Minh - Đảng ủy viên - Thành viên HĐTV Agribank tặng hoa chúc mừng BCH Đảng ủy công ty khóa 4 

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304477/samples/daihoidang/dh1.jpg)

Đội văn nghệ của đoàn thanh niên Công ty biểu diễn văn nghệ chào mừng đại hội