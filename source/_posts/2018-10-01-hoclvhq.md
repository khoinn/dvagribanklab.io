---
title: Đào tạo 'Kỹ năng làm việc hiệu quả'cho cán bộ công nhân viên diễn ra thường xuyên tại Công ty Dịch vụ Agribank
date: 2018-10-01 10:18:40
categories: "hoạt động nội bộ"
tacgia: Phòng Marketing
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288827/samples/2018/lvhq/lvhq_preview.jpg

---

Trong hai ngày nghỉ 29,30/09/2018 tại hội trường Công ty Dịch vụ Agribank đã diễn ra khóa học; bổ sung và trang bị thêm kiến thức về kỹ năng làm việc hiệu quả cho cán bộ, nhân viên Văn phòng công ty và các đơn vị trực thuộc, qua đó thúc đẩy nâng cao hiệu quả công việc, thay đổi tư duy, góp phần cùng lãnh đạo Công ty xây dựng môi trường làm việc chuyên nghiệp.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288826/samples/2018/lvhq/lvhq1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288826/samples/2018/lvhq/lvhq2.jpg)

Chương trình học hai ngày qua, nằm trong chuỗi đào tạo kỹ năng mềm cho cán bộ công nhân viên Công ty dưới sự truyền đạt rất hấp dẫn của thầy ThS.Phan Văn Minh - Giảng viên Đại học Quốc gia.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288826/samples/2018/lvhq/lvhq3.jpg)

Lớp học diễn ra rất sôi động hào hứng với : Kỹ năng giao tiếp và ứng xử nơi công sở, kỹ năng lắng nghe, kỹ năng giải quyết vấn đề. Nội dung được truyền tải thông qua các câu chuyện, các hình ảnh trực quan giúp các học viên nắm bắt bài tốt. Nội dung khóa học đã giúp học viên hình thành thói quen giao tiếp, cách lên kế hoạch và giải quyết công việc hiệu quả.


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288826/samples/2018/lvhq/lvhq4.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543288826/samples/2018/lvhq/lvhq5.jpg)


Các khóa học đào tạo nhằm nâng cao hiệu quả làm việc của Cán bộ công nhân viên là sự quan tâm thường xuyên của Lãnh đạo Công ty cho việc nâng cao chất lượng nguồn nhân lực, cập nhật kiến thức kịp thời hướng tới sự phát triển ổn định của Công ty.