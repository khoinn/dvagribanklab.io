---
title: LỄ XUẤT QUÂN ĐOÀN THỂ THAO CÔNG TY DỊCH VỤ AGRIBANK THAM DỰ HỘI THAO AGRIBANK LẦN THỨ IX NĂM 2022 - KHU VỰC HÀ NỘI.
Date: 2022-6-23
categories: hoạt động nội bộ
tacgia: Kinh doanh Tiếp thị ASC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan_preview.jpg

---
Ngày 23/6/2022, Công ty Dịch vụ Agribank tổ chức Lễ xuất quân đoàn thể thao tham dự Hội thao Agribank lần thứ IX năm 2022 - Khu vực Hà Nội.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan1.jpg)
*Đồng chí Trần Hồng Thắng - Phó Tổng Giám đốc Phụ trách điều hành, Chủ tịch Công đoàn Công ty phát biểu chỉ đạo tại buổi lễ.*

Đến dự Lễ xuất quân có đồng chí Trần Hồng Thắng - Phó Tổng Giám đốc Phụ trách điều hành, Chủ tịch Công đoàn Công ty Dịch vụ Agribank; đồng chí Phạm Ngọc Ruyển - Giám đốc Nhà in Ngân hàng I; đồng chí Bùi Duy Thanh - Giám đốc Trung tâm Dịch vụ ngân quỹ cùng sự tham gia của các huấn luyện viên, đội trưởng và 44 vận động viên tham gia Hội thao Agribank với 6 môn thi đấu: bóng đá, bóng chuyền da nam, bóng chuyền hơi nữ, cầu lông, bóng bàn và tennis.

Phát biểu tại Lễ xuất quân, đồng chí Trần Hồng Thắng - Phó Tổng Giám đốc Phụ trách điều hành, Chủ tịch Công đoàn Công ty đánh giá cao sự nỗ lực của các huấn luyện viên, vận động viên đã khắc phục mọi khó khăn, tập luyện với tinh thần quyết tâm và ý chí quyết thắng, sẵn sàng thi đấu giành thành tích cao tại Hội thao Agribank lần thứ IX năm 2022 - Khu vực Hà Nội.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan2.jpg)
*Đồng chí Nguyễn Việt Cường - Trưởng đoàn thể thao phát biểu tại buổi Lễ.*

Thay mặt đoàn thể thao, đồng chí Nguyễn Việt Cường, trưởng đoàn thể thao thể hiện lòng quyết tâm, đoàn kết, giữ vững kỷ luật, nỗ lực hoàn thành mục tiêu đã đề ra; thi đấu với tinh thần thể thao cao thượng, ngoan cường, trung thực, phấn đấu đạt thành tích top đầu toàn đoàn thể thao khu vực Hà Nội, góp phần nâng cao vị thế và khẳng định được truyền thống thể thao của Công ty trong hệ thống Agribank.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan4.jpg)

Hội thao Agribank là sự kiện thể thao quy tụ các vận động viên là cán bộ nhân viên của Ngân hàng Agribank nhằm nâng cao sức khoẻ, thúc đẩy tình đoàn kết, mối quan hệ tốt đẹp, sự hiểu biết lẫn nhau giữa các Chi nhánh, Công ty trong Agribank./.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan5.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1655976983/samples/2022/lexuatquan/lexuatquan6.jpg)






