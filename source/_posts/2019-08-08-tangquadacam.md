---
title: Công ty Dịch vụ Agribank tham gia mít tinh kỷ niệm 58 năm thảm họa da cam ở Việt Nam và tặng quà cho nạn nhân chất độc da cam tại phường Thanh Xuân Trung-Quận Thanh Xuân-Thành phố Hà Nội.
Date: 2019-8-8 15:10:35
categories: hoạt động nội bộ
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1565234731/samples/2019/dacam2019/dacam2019_preview.jpg
---
Kỷ niệm 58 năm thảm họa da cam ở Việt Nam 10/8/1961 – 10/8/2019, sáng 8/8/2019 Công ty Dịch vụ Agribank đồng hành phối hợp với Hội nạn nhân chất độc da cam phường Thanh Xuân Trung - Quận Thanh Xuân -Thành phố Hà Nội Hà Nội tổ chức chương trình thăm hỏi tặng quà các gia đình nạn nhân chất độc da cam, tặng 20 xuất quà tổng giá trị 10 triệu đồng cho 20 gia đình có nạn nhân chất độc da cam trên địa bàn quận Thanh Xuân.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1565234731/samples/2019/dacam2019/dacam2019_1.jpg)
*Ông Đỗ Đình Hồng Chủ tịch-Tổng Giám đốc Công ty Dịch vụ Agribank phát biểu tại buổi lễ.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1565234731/samples/2019/dacam2019/dacam2019_2.jpg)
*Đồng chí Đỗ Đình Hồng trao quà cho các gia đình có nạn nhân chất độc da cam*

Tới dự lễ, đại diện Công ty Dịch vụ Agribank có ông Đỗ Đình Hồng Chủ tịch -Tổng Giám đốc Công ty, ban Tổng Giám đốc, ban chấp hành Công đoàn cùng Đoàn Thanh niên CSHCM. Về phía phường Thanh Xuân Trung- Quận Thanh Xuân có ông Nguyễn Hữu Thọ - Phó bí thư Đảng ủy, Chủ tịch UBND phường, ông Lê Quang Lâm - Chủ tịch Hội nạn nhân chất độc da cam phường và 20 gia đình các nạn nhân chất độc da cam có hoàn cảnh đặc biệt khó khăn trên địa bàn.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1565234729/samples/2019/dacam2019/dacam2019_3.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1565234732/samples/2019/dacam2019/dacam2019_4.jpg)

Việc tặng quà nằm trong chương trình hoạt động thường niên của Công ty, xuất phát từ sự thấu hiểu nỗi đau thương và mất mát của các gia đình nạn nhân, chung tay với cộng đồng và xã hội.



