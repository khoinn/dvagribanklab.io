---
title: Lễ Bế giảng Chương trình đào tạo "Nâng cao năng lực quản trị dành cho cán bộ quản lý Công ty Dịch vụ Agribank"
categories: hoạt động nội bộ
date: 2017-05-08 15:10:35
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304123/samples/2017/taphuan2/th2_preview.jpg

---

Trong 2 ngày cuối tuần mùng 6 và 7/5/2017 đã diễn ra 2 buổi học cuối cùng trong chương trình đào tạo: "Nâng cao năng lực quản trị dành cho cán bộ quản lý Công ty Dịch vụ Agribank (ABSC)". Bộ giáo trình sử dụng cho Chương trình đào tạo lần này được Khoa Quản trị và Kinh doanh – Đại học Quốc gia Hà Nội thiết kế dành riêng cho ABSC.


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304120/samples/2017/taphuan2/hinh1.jpg)

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304120/samples/2017/taphuan2/hinh2.jpg)

*Toàn cảnh lớp học ngày 07/5 tại Công ty Dịch vụ Agribank.*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1543304120/samples/2017/taphuan2/hinh3.jpg)

*Lễ Bế giảng Chương trình đào tạo: "Nâng cao năng lực quản trị dành cho cán bộ quản lý Công ty Dịch vụ Agribank (ABSC)*

Đây là một trong những kế hoạch được Ban lãnh đạo xây dựng với mục tiêu phát triển Công ty ngày càng lớn mạnh, đáp ứng tốt nhiệm vụ mới của ngành và Nhà nước.
Kết thúc khóa học các học viên làm bài kiểm tra đánh giá kết quả và được cấp Chứng chỉ hoàn thành Chương trình đào tạo.
