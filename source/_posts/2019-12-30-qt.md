---
title: Công ty Dịch vụ Agribank với công tác quyết toán cuối năm 2019
categories: hoạt động nội bộ
date: 2019-12-30
tacgia: Marketing ABSC
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1577756657/samples/2019/qt/qt1_preview.jpg
---

Trong không khí sôi nổi thi đua hoàn thành kế hoạch năm 2019 và chuẩn bị triển khai kế hoạch năm 2020. Chiều ngày 30/12/2019, tại Công ty Dịch vụ Agribank đã đón đoàn làm việc của Ngân hàng Nông nghiệp và Phát triển Nông thôn Việt Nam, đại diện do đồng chí Phạm Hoàng Đức - Thành viên HĐTV, Điều hành HĐTV Agribank đến làm việc, tặng quà, khích lệ động viên kịp thời Ban Lãnh đạo và cán bộ công nhân viên Công ty Dịch vụ Agribank trong những ngày cuối năm thực hiện công tác quyết toán.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1577756660/samples/2019/qt/qt2.jpg)

*Đồng chí Phạm Hoàng Đức - Thành viên HĐTV, Điều hành HĐTV Agribank làm việc với Ban lãnh đạo, cán bộ chủ chốt của Công ty Dịch vụ Agribank*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1577756660/samples/2019/qt/qt1.jpg)

*Đồng chí Đỗ Đình Hồng Chủ tịch-Tổng Giám đốc báo cáo kết quả hoạt động của Công ty Dịch vụ Agribank*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1577756660/samples/2019/qt/qt3.jpg)

*Đồng chí Phạm Hoàng Đức thay mặt lãnh đạo Agribank Chúc Mừng  Năm Mới và tặng quà Công ty*

Năm 2019 Công ty Dịch vụ Agribank được ghi nhận có nhiều cố gắng tích cực vượt khó trong hoạt động sản xuất kinh doanh. Được sự quan tâm chỉ đạo sâu sát của lãnh đạo các cấp cùng với sự phát huy nội lực, đoàn kết của tập thể CBCNV, công ty tiếp tục gặt hái nhiều thành công hơn trong năm 2020.
