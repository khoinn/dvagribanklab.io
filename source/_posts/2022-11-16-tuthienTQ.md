---
title: ĐOÀN THANH NIÊN CÔNG TY DỊCH VỤ AGRIBANK PHỐI HỢP THỰC HIỆN CHƯƠNG TRÌNH TÌNH NGUYỆN MÙA ĐÔNG NĂM 2022 - XUÂN TÌNH NGUYỆN 2023

categories: hoạt động nội bộ
date: 2022-11-16
tacgia: Kinh doanh Tiếp thị
preview: https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang_preview.jpg

---

<p align="justify">Ngày 13/11/2022 tại Trường Tiểu học và Trung học cơ sở Đông Thọ - huyện Sơn Dương, tỉnh Tuyên Quang, Đoàn Thanh niên Công ty Dịch vụ Agribank phối hợp với Huyện Đoàn Sơn Dương, Hội LHTN Việt Nam huyện Sơn Dương, Chi đoàn Agribank tỉnh Tuyên Quang và Đoàn cơ sở Trại giam Quyết Tiến tổ chức ra quân thực hiện chương trình Tình nguyện mùa Đông năm 2022 và Xuân tình nguyện 2023.<p>

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang1.jpg)
*Đại biểu tham gia chương trình Tình nguyện mùa Đông năm 2022 và Xuân tình nguyện 2023*

Tham dự chương trình có đồng chí Lý Văn Dũng - Ủy viên ủy ban Hội LHTN Việt Nam tỉnh Tuyên Quang, chuyên viên tỉnh đoàn Tuyên Quang; đồng chí Trần Thị Hoàn – Bí thư Huyện đoàn Sơn Dương; các đồng chí đại diện cấp ủy, chính quyền địa phương; Ban giám hiệu nhà trường cùng toàn thể các em học sinh trường TH & THCS Đông Thọ. Về phía Agribank có đồng chí Đào Quang Uy – Phó Giám đốc Agribank Chi nhánh tỉnh Tuyên Quang, Giám đốc và Phó Giám đốc Agribank Chi nhánh huyện Sơn Dương, Bí thư và Phó bí thư Chi đoàn Agribank tỉnh Tuyên Quang, Agribank Sơn Dương cùng các đồng chí đoàn viên thanh niên Agribank Chi nhánh tỉnh Tuyên Quang.
Về phía Công ty Dịch vụ Agribank, có đồng chí Trần Hồng Thắng – Phó Bí thư Đảng ủy, Phó Tổng Giám đốc Phụ trách điều hành; đồng chí Trần Thu Thủy – Phó Tổng Giám đốc và các đoàn viên thanh niên Công ty.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang2.jpg)
*Đồng chí Trần Hồng Thắng - Phó Bí thư Đảng ủy, Phó Tổng Giám đốc PTĐH Công ty và đồng chí Trần Thu Thủy - Phó Tổng Giám đốc trao biển tượng trưng kinh phí quà tặng với tổng giá trị 40 triệu đồng cho đại diện xã Đông Thọ, huyện Sơn Dương, tỉnh Tuyên Quang*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang3.jpg)
*Đồng chí Trần Hồng Thắng - Phó Bí thư Đảng ủy, Phó Tổng Giám đốc PTĐH Công ty trao tặng quà cho các hộ gia đình có hoàn cảnh khó khăn thuộc xã Đông Thọ*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang4.jpg)
*Đồng chí Trần Vũ Hải Hà - Bí thư Đoàn thanh niên Công ty trao tặng 15 bộ chăn gối và 15 màn cho các em học sinh trường Mầm non xã Đông Thọ*

Tại chương trình, Ban thường vụ Huyện đoàn, Ủy ban Hội LHTN huyện Sơn Dương, Đoàn cơ sở Trại giam Quyết Tiến, Chi đoàn Agribank tỉnh Tuyên Quang, các cơ sở Đoàn khối HCSN, DN, LLVT trên địa bàn huyện Sơn Dương đã trao tặng 26 triệu đồng hỗ trợ cho thôn Tân An, xã Đông Thọ xây dựng 350m đường bê tông nông thôn.
Nhằm hưởng ứng chương trình Tình nguyện mùa Đông 2022 và Xuân tình nguyện 2023, Công ty Dịch vụ Agribank đã trao tặng 20 suất quà cho 20 hộ gia đình nghèo đặc biệt khó khăn và 36 bộ chăn, ga, gối; 15 chiếc màn cho học sinh trường TH & THCS Đông Thọ, trường Mầm non Đông Thọ... với tổng giá trị 40 triệu đồng.

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang5.jpg)
*Đoàn thanh niên Công ty Dịch vụ Agribank phối hợp với Đoàn thanh niên Agribank tỉnh Tuyên Quang chuẩn bị quà cho các em học sinh và các hộ gia đình có hoàn cảnh đặc biệt khó khăn*

![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang6.jpg)
*Các đồng chí đại diện Huyện Đoàn Sơn Dương, Đoàn cơ sở trại giam Quyết Tiến, Hội liên hiệp thanh niên Việt Nam huyện Sơn Dương, Chi đoàn Agribank tỉnh Tuyên Quang và Công ty Dịch vụ Agribank chụp ảnh lưu niệm cùng cán bộ và nhân dân xã Đông Thọ*


![](https://res.cloudinary.com/dich-vu-agribank/image/upload/v1668680578/samples/2022/tuthientuyenquang/tuyenquang7.jpg)
*Các đồng chí đại diện Huyện Đoàn Sơn Dương, Đoàn cơ sở trại giam Quyết Tiến, Hội liên hiệp thanh niên Việt Nam huyện Sơn Dương, Chi đoàn Agribank tỉnh Tuyên Quang và Công ty Dịch vụ Agribank chụp ảnh lưu niệm cùng cán bộ và nhân dân xã Đông Thọ*

Qua chương trình hết sức ý nghĩa này, Ban lãnh đạo Công ty Dịch vụ Agribank mong muốn phát huy vai trò của đoàn viên thanh niên trong việc tham gia phong trào tình nguyện vì cuộc sống cộng đồng, góp phần đảm bảo an sinh xã hội, chăm lo đời sống của nhân dân, đặc biệt là những hộ gia đình có hoàn cảnh khó khăn trên địa bàn xã Đông Thọ. 
Công ty Dịch vụ Agribank sẽ tiếp tục duy trì, hưởng ứng và tích cực phát động triển khai các hoạt động tình nguyện nhằm phát huy truyền thống tương thân tương ái của dân tộc. 
“Đông về - Cùng ASC trao tặng yêu thương” 











