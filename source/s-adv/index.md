---
title: s_adv
date: 2018-06-21 12:58:39
---

### Các dịch vụ chính:

Thiết kế ý tưởng biển quảng cáo tấm lớn ngoài trời

Thiết kế, thi công biển hiệu Chi nhánh, các phòng Giao dịch trong hệ thống

Quảng cáo Thương hiệu Agribank trên nóc các tòa nhà cao tầng các Chi nhánh

Thiết kế thi công gian hàng triển lãm, hội chợ

Thiết kế thi công lắp đặt cabin ATM tại Chi nhánh và các phòng Giao dịch

### Các sản phẩm đã thực hiện trong và ngoài hệ thống Agribank:

Năm 2008: Quảng cáo biển tấm lớn cho Agribank và tập đoàn Fujitsu
Hiện nay dịch vụ quảng cáo đang quảng cáo cho; Công ty Bigsun sản phẩm Sơn Hà Nội, Công ty Bizman sản phẩm Dệt may Việt Nam tại biển quảng cáo tấm lớn trên trục đường Hà Nội - Nội Bài.

Thiết kế và thi công biển quảng cáo thương hiệu Agribank trên nhà cao tầng: Số 2 Láng Hạ, Chi nhánh Gia Lâm, Ocean Park...

Thiết kế, tổ chức thi công biển hiệu nhiều Chi nhánh trong hệ thống ở địa bàn Hà Nội, trong đó có Trụ sở chính Agribank

Tổ chức thi công trên 300 cabin ATM trong hệ thống.

Nhiều năm thực hiện thiết kế và tổ chức thi công triển lãm Công nghệ Thông tin Agribank, và nhiều lần được kỷ niệm chương gian hàng đẹp nhất do tổ chức IDG (Mỹ) bình chọn.
