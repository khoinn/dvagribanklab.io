---
title: s_prt
date: 2018-06-21 12:58:26
---

### Các dịch vụ chính:

In séc, cổ phiếu, trái phiếu, sổ tiết kiệm, giấy biên lai, nhật ký ATM, xổ số...

Nhãn mác, bao bì hàng hoá trên giấy, màng nhôm...

Các sản phẩm tờ rời phục vụ công tác nghiệp vụ của hệ thống ngân hàng.

Tạp chí, lịch, tờ gấp, poster...

Đặc biệt, với uy tín và kinh nghiệm của mình, Công ty được Ngân hàng Nhà nước giao in ấn chỉ đặc biệt.

### Các sản phẩm đặc biệt:

In ấn các sản phẩm đặc biệt có độ an toàn cao, bảo mật. 

Chống giả bằng giấy đặc biệt, màu phát quang, hoa văn vẽ đồ họa phức tạp. Dưới sự giám sát khắt khe và kiểm tra kỹ thuật chặt chẽ