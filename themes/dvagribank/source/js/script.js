$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 600) {
    $('.go-up').fadeIn();
  } else {
    $('.go-up').fadeOut();
  }
});

$(document).ready(function(){
  $('.client_comm').slick({
    infinite: true,
    autoplay: true,
    arrows: false
  });
});